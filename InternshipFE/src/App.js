import "./App.scss";
import LeftNavbarTopNavbarLayout from "./layouts/leftNavbarTopNavbarLayout/LeftNavbarTopNavbarLayout";
import { Route, Routes } from "react-router-dom";
import Categories from "./pages/categories/Categories";
import ProfileCard from "./components/profile/ProfileCard";
import ViewPost from "./pages/viewPost/ViewPost";
import MyPosts from "./pages/myPosts/MyPosts";
import MyFavouritePosts from "./pages/myFavouritePosts/MyFavouritePosts";
import NewPostPage from "./pages/postFormPage/newPostPage/NewPostPage";
import Register from "./components/register/Register";
import SubcategoryPage from "./pages/subcategoryPage/SubcategoryPage";
import Login from "./pages/loginPage/Login";
import EditPostPage from "./pages/postFormPage/editPostPage/EditPostPage";
import TopNavbar from "./components/topNavbar/TopNavbar";
import RequireAuth from "./components/requireAuth/RequireAuth";

function App() {

  const layout = (
    <LeftNavbarTopNavbarLayout
      topNavbar={<TopNavbar /> /* INSERT YOUR TOP NAVBAR HERE */}
    >
      {/* INSERT YOUR PAGES OR ANY OTHER CONTENT HERE, BETWEEN THESE 2 TAGS 
        PS: CHECK LeftNavbar COMPONENT FOR ROUTE PATHS */}
      <Routes>
        <Route path="/profile" element={<ProfileCard />}></Route>
        <Route path="/categories" element={<Categories />}></Route>
        <Route path="/categories/:category" element={<SubcategoryPage />} />
        <Route path="/post/edit/:postId" element={<EditPostPage />} />
        <Route path="/newpost" element={<NewPostPage />} />
        <Route path="/myposts" element={<MyPosts />} />
        <Route path="/myfavourites" element={<MyFavouritePosts />} />
        <Route path="/post/view/:id" element={<ViewPost />} />
      </Routes>
    </LeftNavbarTopNavbarLayout>
  );

  return (
    <div>
      <Routes>
        <Route
          path="/login"
          element={<Login></Login>}
        />
        <Route
          path="/register"
          element={<Register/>}
        />
        <Route path="*" element={<RequireAuth>{layout}</RequireAuth>} />
      </Routes>
    </div>
  );
}
export default App;
