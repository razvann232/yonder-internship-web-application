import React from 'react'

const NoAccessPage = () => {
  return (
    <div>You have no access to this page. Please, go to the login page!</div>
  )
}

export default NoAccessPage