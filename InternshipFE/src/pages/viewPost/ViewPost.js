import { React, useEffect, useState, useRef } from "react";
import Like from "../../assets/Icons/like.svg";
import LikeFilled from "../../assets/Icons/like-filled-white.svg";
import TimerPic from "../../assets/Icons/timer.svg";
import UserIcon from "../../assets/Icons/user.svg";
import EmailIcon from "../../assets/Icons/mail.svg";
import PhoneIcon from "../../assets/Icons/phone.svg";
import "./ViewPost.scss";
import Comment from "../../components/comment/Comment";
import ImageSlider from "../../components/imageSlider/ImageSlider";
import { useParams } from "react-router-dom";
import RequestHelper from "../../components/requestHelper/RequestHelper";
import DateBeautifier from "../../components/dateBeautifier/DateBeautifier";
import { CONFIG } from "../../config/app.config";
import SessionStorage from "../../components/sessionStorage/SessionStorage.ts";
import Modal from "../../components/modal/Modal";
import DeauthHandler from "../../components/deauthHandler/DeauthHandler";
import ErrorHandler from "../../components/errorHandler/ErrorHandler";

function ViewPost() {
  const postId = useParams().id;

  const [likePic, setLikePic] = useState(Like);
  const [message, setMessage] = useState("");
  const [postData, setPostData] = useState([]);
  const [loaded, setLoaded] = useState(false);
  const storage = SessionStorage();

  const token = CONFIG.PRODUCTION
    ? storage.getItem("token")
    : CONFIG.DEFAULT_DEBUGGING_USER_ID.toString();
  const [error, setError] = useState(null);
  const deauthHandler = DeauthHandler();

  const fillLike = () => {
    if (likePic === Like) {
      setLikePic(LikeFilled);
      registerLike();
    } else {
      unregisterLike();
      setLikePic(Like);
    }
  };

  const registerLike = () => {
    RequestHelper("http://localhost:8080").POST(
      "/post/like/" + postId,
      { auth: token },
      {},
      (data, error) => {
        if (error) {
          ErrorHandler(error, setError, deauthHandler);
          return;
        }
      }
    );
  };

  const unregisterLike = () => {
    RequestHelper("http://localhost:8080").DELETE(
      "/post/unlike/" + postId,
      { auth: token },
      (data, error) => {
        if (error) {
          ErrorHandler(error, setError, deauthHandler);
          return;
        }
      }
    );
  };

  const getPostData = () => {
    RequestHelper("http://localhost:8080").GET(
      "/post/" + postId,
      { auth: token },
      (data, error) => {
        if (error) {
          ErrorHandler(error, setError, deauthHandler);
          return;
        }
        if (data.data.length === 0) {
          setMessage("No posts found!");
        } else {
          const newData = data?.data;
          newData.comments = newData.comments?.sort((a, b) => b.id - a.id);
          setPostData(newData);
          setLoaded(true);
        }
      }
    );
  };

  useEffect(() => {
    getPostData();

  }, []);

  useEffect(() => {
    setLikePic(postData.liked ? LikeFilled : Like);
  }, [postData.liked]);

  const ref = useRef(null);
  const addComment = () => {
    if (ref.current.value === "") {
      const error = {
        title: "Comentariu invalid",
        message: "Comentariile trebuie sa contina text!",
      };
      setError(error);
      return;
    }

    RequestHelper("http://localhost:8080").POST(
      "/comment/save/" + postId,
      { auth: token },
      { content: ref.current.value },
      (data, error) => {
        if (error) {
          ErrorHandler(error, setError, deauthHandler);
          return;
        }
        getPostData();
        ref.current.value = "";
      }
    );
  };

  const returnContent = () => {
    if (message !== "") {
      return <span className="error-text">{message}</span>;
    }

    return (
      loaded && (
        <div className="view-post-page">
          <ImageSlider slides={postData.images} className="view-post-images" />
          <div className="view-post-info">
            <div className="header-vp">
              <span className="title-vp">{postData.title}</span>
              <div className="time-price-vp">
                <img src={TimerPic} alt="sign" />
                <span className="time-vp">
                  {DateBeautifier().beautifyPostDate(postData)}
                </span>
                <span className="price-vp">
                  {postData.category?.type === "SERVICES"
                    ? "Multiple prices"
                    : postData.price === 0
                    ? "Negotiable"
                    : postData.price + "RON"}
                </span>
                <img
                  className="like-vp"
                  src={likePic}
                  alt="heart-sign"
                  onClick={fillLike}
                />
              </div>
            </div>
            <span className="small-header">Description</span>
            <span className="desc-vp">{postData.description}</span>
            <div className="data-main-container">
              <div className="data-second-container">
                <span className="small-header">Contact seller</span>
                <div className="data-container">
                  <img
                    src={UserIcon}
                    alt="user-icon"
                    className="data-icon-vp"
                  />
                  <span className="data-text">{postData.user?.name}</span>
                </div>
                <div className="data-container">
                  <img
                    src={EmailIcon}
                    alt="mail-icon"
                    className="data-icon-vp"
                  />
                  <span className="data-text">{postData.user?.email}</span>
                </div>
                <div className="data-container">
                  <img
                    src={PhoneIcon}
                    alt="phone-icon"
                    className="data-icon-vp"
                  />
                  <span className="data-text">
                    {postData.user?.phoneNumber}
                  </span>
                </div>
              </div>

              <div className="data-second-container">
                <span className="small-header">Added on</span>
                <span className="data-text">
                  {postData.createdAt?.substring(0, 10)}
                </span>
                <span className="small-header">Expires on</span>
                <span className="data-text">
                  {postData.expiresAt?.substring(0, 10)}
                </span>
              </div>

              <div className="data-second-container">
                <span className="small-header">Category</span>
                <span className="data-text">{postData.category?.type}</span>
                <span className="small-header">Views</span>
                <span className="data-text">{postData.viewsNo}</span>
              </div>
            </div>
            <div className="comments-container">
              <div className="header">
                <span className="title">Comments</span>
              </div>
              <span className="small-header">User Comments</span>
              <div className="comment-list">
                {postData.comments?.map((comment) => {
                  return (
                    <Comment data={comment} key={`comm_id_${comment.id}`} />
                  );
                })}
              </div>
            </div>
            <span className="small-header-comment">Add comments</span>
            <textarea ref={ref} className="comment-input" />
            <button className="add-comment-button" onClick={addComment}>
              Add comment
            </button>
          </div>
        </div>
      )
    );
  };

  return (
    <>
      {error && (
        <Modal
          title={error.title}
          message={error.message}
          buttons={error.buttons}
          onExit={
            error.onExit
              ? error.onExit
              : () => {
                  setError(null);
                }
          }
        />
      )}
      <div>{returnContent()}</div>
    </>
  );
}

export default ViewPost;
