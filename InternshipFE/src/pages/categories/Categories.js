import React, { useState } from "react";
import { Link } from "react-router-dom";
import ItemBox from "../../components/itemBox/ItemBox";
import SellAndBuy from "../../assets/Images/PNG/SellAndBuy.png";
import Exchange from "../../assets/Images/PNG/Exchange.png";
import Loans from "../../assets/Images/PNG/Loans.png";
import Services from "../../assets/Images/PNG/Services.png";
import "./Categories.scss";
import RequestHelper from "../../components/requestHelper/RequestHelper";
import ErrorHandler from "../../components/errorHandler/ErrorHandler";
import DeauthHandler from "../../components/deauthHandler/DeauthHandler";
import { CONFIG } from "../../config/app.config" 
import SessionStorage from "../../components/sessionStorage/SessionStorage.ts"

function Categories() {
  const [postCounts, setPostCounts] = useState([]);
  const [message, setMessage] = useState("");
  const [error, setError] = useState(null);
  const deauthHandler = DeauthHandler();
  const storage = SessionStorage();
  const token = CONFIG.PRODUCTION ? storage.getItem("token") : CONFIG.DEFAULT_DEBUGGING_USER_ID.toString();

  useState(()=>{
    RequestHelper("http://localhost:8080").GET(
      "/post/count",
      { auth: token },
      (data, error) => {
        if (error) {
          ErrorHandler(error, setError, deauthHandler);
          return;
        }
        if (data.data.length === 0) {
          setMessage("No posts found!");
        } else {
          setPostCounts(data.data);
        }
      }
    );
  }, [])

  return (
    <div className="main-container">
      <Link to="/categories/sell">
        <ItemBox newItems={postCounts.numberSellPosts ? postCounts.numberSellPosts : "No"} image={SellAndBuy} title="Sell & Buy" />
      </Link>
      <Link to="/categories/exchange">
        <ItemBox newItems={postCounts.numberExchangePosts ? postCounts.numberExchangePosts : "No"} image={Exchange} title="Exchange" />
      </Link>
      <Link to="/categories/loans">
        <ItemBox newItems={postCounts.numberLoansPosts ? postCounts.numberLoansPosts : "No"} image={Loans} title="Loans" />
      </Link>
      <Link to="/categories/services">
        <ItemBox newItems={postCounts.numberServicesPosts ? postCounts.numberServicesPosts : "No"} image={Services} title="Services" />
      </Link>
    </div>
  );
}

export default Categories;
