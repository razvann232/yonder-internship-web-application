import "./MyFavouritePosts.scss";
import { React, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import RequestHelper from "../../components/requestHelper/RequestHelper";
import FavouriteCard from "./FavouriteCard";
import { CONFIG } from "../../config/app.config";
import SessionStorage from "../../components/sessionStorage/SessionStorage.ts";

const MyFavouritePosts = () => {
  const [posts, setPosts] = useState([]);
  const [message, setMessage] = useState("");
  const navigate = useNavigate();
  const storage = SessionStorage();
  const token = CONFIG.PRODUCTION
    ? storage.getItem("token")
    : CONFIG.DEFAULT_DEBUGGING_USER_ID.toString();

  useEffect(() => {
    RequestHelper("http://localhost:8080").GET(
      "/post/my-favourites",
      { auth: token },
      (data, error) => {
        if (error) {
          if (error.handleable) {
            if (error.code === 401) {
              navigate("/login");
            }
          } else {
            setMessage("Error connecting to the server!");
          }
        } else {
          if (data.data.length === 0) {
            setMessage("No such favourite posts found.");
          } else {
            setPosts(data.data);
          }
        }
      }
    );
  }, []);

  const displayTime = (post) => {
    if (
      post.active === false ||
      post.timeLeftDays < 0 ||
      post.timeLeftHours < 0 ||
      post.timeLeftDays < 0
    ) {
      return "Expired";
    }

    if (post.timeLeftDays === 0) {
      if (post.timeLeftHours === 0) {
        return post.timeLeftMinutes + "min";
      }
      return post.timeLeftHours + "h" + posts.timeLeftMinutes + "min";
    }
    return (
      post.timeLeftDays +
      "d" +
      post.timeLeftHours +
      "h" +
      post.timeLeftMinutes +
      "min"
    );
  };

  const determinePrice = (price) => {
    if (price === 0) return;
    return price;
  };

  const getPostsByStatus = (status) => {
    if (message !== "") {
      return <span className="no-posts-text-fav">{message}</span>;
    }
    return posts
      .filter((post) => post.active === status)
      .map((post) => (
        <FavouriteCard
          image={
            "data:" + post.thumbnail.type + ";base64," + post.thumbnail.content
          }
          title={post.title}
          time={displayTime(post)}
          price={determinePrice(post.price)}
          postId={post.id}
          isActive={post.active}
          key={`fav_card_id_${post.id}`}
        />
      ));
  };

  return (
    <div>
      <div className="myfavourites-navbar-fav">
        <span
          className="myposts-nav-text-fav"
          onClick={() => navigate("/myposts")}
        >
          My Posts
        </span>
        <span className="myfavs-nav-text-fav">My Favourites</span>
      </div>
      {getPostsByStatus(true).length ? (
        <>
          <span className="status-posts-label-fav">Active</span>
          <div className="myposts-container-fav">{getPostsByStatus(true)}</div>
        </>
      ) : null }
      {getPostsByStatus(false).length ? (
        <>
          <span className="status-posts-label-fav">Inactive</span>
          <div className="myposts-container-fav">{getPostsByStatus(false)}</div>
        </>
      ) : null }
    </div>
  );
  
};

export default MyFavouritePosts;
