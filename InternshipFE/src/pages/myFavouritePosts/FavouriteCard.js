import React, { useState } from "react";
import "./FavouriteCard.scss";
import TimerPic from "../../assets/Icons/timer.svg";
import LikePicFilled from "../../assets/Icons/like-filled-white.svg";
import { useNavigate } from "react-router-dom";

function FavouriteCard({ image, title, time, price, postId, isActive }) {
  const [likePic, setLikePic] = useState(LikePicFilled);
  const [message, setMessage] = useState("");
  const navigate = useNavigate();

  const displayCard = () => {
    if (message !== "") {
      return <span className="error-text-fav">{message}</span>;
    }

    return (
      <div
        onClick={() => {
          navigate("/post/view/" + postId);
        }}
      >
        <div className="card-images-fav">
          <img
            className={`product-image-fav product-image-fav--${
              isActive ? "active" : "inactive"
            }`}
            src={image}
            alt="product-image"
          ></img>
          <img className="like-icon-card-fav" src={likePic} alt="heart-sign" />
        </div>

        <span
          className={`card-title-fav card-title-fav--${
            isActive ? "active" : "inactive"
          }`}
        >
          {title}
        </span>

        <div className="time-price-fav">
          <img
            src={TimerPic}
            alt="timer-sign"
            className={`timer-pic-fav timer-pic-fav--${
              isActive ? "active" : "inactive"
            }`}
          />
          <span
            className={`time-fav time-fav--${isActive ? "active" : "inactive"}`}
          >
            {time}
          </span>

          <span
            className={`price-fav price-fav--${
              isActive ? "active" : "inactive"
            }`}
          >
            {price ? price + " RON" : ""}
          </span>
        </div>
      </div>
    );
  };

  return <div className="card-fav">{displayCard()}</div>;
}

export default FavouriteCard;
