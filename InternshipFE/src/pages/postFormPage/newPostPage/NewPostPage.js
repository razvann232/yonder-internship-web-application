import React, { useEffect, useState } from "react";
import NewPostForm from "../../../components/newPostForm/NewPostForm";
import TextButton from "../../../components/textButton/TextButton";
import styles from "../postFormPageStyle.module.scss";
import RequestHelper from "../../../components/requestHelper/RequestHelper";
import Modal from "../../../components/modal/Modal";
import InputValidators from "../InputValidators";
import { useNavigate } from "react-router-dom";
import { categoryURLs } from "../../../config/app.config";
import {CONFIG} from "../../../config/app.config"
import SessionStorage from "../../../components/sessionStorage/SessionStorage.ts";
import ErrorHandler from "../../../components/errorHandler/ErrorHandler";
import DeauthHandler from "../../../components/deauthHandler/DeauthHandler";

const NewPostPage = () => {
  const HTTP = RequestHelper(CONFIG.SERVER_ADRESS);
  const navigate = useNavigate();
  const [errors, setErrors] = useState(null);
  const storage = SessionStorage();
 

  const generateImageArray = (numberOfImages) => {
    const arrayOfImages = [];
    for (let i = 0; i < numberOfImages; i++) {
      arrayOfImages.push({ id: i, image: null });
    }
    return arrayOfImages;
  };

  const NUMBER_OF_IMAGES = 9;
  const imagesBoilerplate = generateImageArray(NUMBER_OF_IMAGES);
  const dataBoilerplate = {
    title: "",
    description: "",
    price: "",
    category: "",
    date: "",
    views: "",
  };

  const [postImages, setPostImages] = useState(imagesBoilerplate);
  const [postData, setPostData] = useState(dataBoilerplate);

  const sendRequest = () => {
    const errorsMessages = InputValidators(postData);
    if (errorsMessages.length) {
      setErrors({
        title: "Eroare! Unul dintre campuri nu este valid!",
        message: errorsMessages,
        buttons: null,
      });
    } else {
      const dateObj = new Date(postData.date);
      const body = {
        title: postData.title,
        description: postData.description,
        price: postData.price,
        category: postData.category,
        expiresAt: dateObj.toISOString(),
        images: postImages.map((imageObj) => imageObj.image),
      };
      const token = CONFIG.PRODUCTION ? storage.getItem("token") : CONFIG.DEFAULT_DEBUGGING_USER_ID.toString();
      HTTP.formDataPOST("/post/save", { auth: token }, body, (data, error) => {
        if (error) {
          if (error) {
            ErrorHandler(error, setErrors, DeauthHandler());
            return;
          }
        } else {
          navigate(categoryURLs[body.category]);
        }
      });
    }
  };

  return (
    <>
      {errors && (
        <Modal
          title={errors.title}
          message={errors.message}
          buttons={errors.buttons}
          onExit={
            errors.onExit
              ? errors.onExit
              : () => {
                  setErrors(null);
                }
          }
        />
      )}
      <NewPostForm
        postImages={postImages}
        setPostImages={setPostImages}
        postData={postData}
        setPostData={setPostData}
      />
      <div className={styles.buttonContainer}>
        <TextButton
          className={styles.button}
          type={"secondary"}
          onClick={() => navigate(-1)}
          text={"Cancel"}
        />
        <TextButton
          className={styles.button}
          type={"primary"}
          text={"Save Post"}
          onClick={sendRequest}
        />
      </div>
    </>
  );
};

export default NewPostPage;
