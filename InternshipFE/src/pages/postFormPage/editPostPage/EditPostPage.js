import React, { useEffect, useState } from "react";
import NewPostForm from "../../../components/newPostForm/NewPostForm";
import TextButton from "../../../components/textButton/TextButton";
import styles from "../postFormPageStyle.module.scss";
import RequestHelper from "../../../components/requestHelper/RequestHelper";
import Modal from "../../../components/modal/Modal";
import InputValidators from "../InputValidators";
import { useNavigate, useParams } from "react-router-dom";
import { categoryURLs } from "../../../config/app.config";
import { CONFIG } from "../../../config/app.config";
import DeauthHandler from "../../../components/deauthHandler/DeauthHandler";
import ErrorHandler from "../../../components/errorHandler/ErrorHandler";
import SessionStorage from "../../../components/sessionStorage/SessionStorage.ts";
import Base64toFILE from "../../../components/base64toFILE/Base64toFILE";

const EditPostPage = () => {
  const HTTP = RequestHelper(CONFIG.SERVER_ADRESS);
  const navigate = useNavigate();
  const [errors, setErrors] = useState(null);
  const params = useParams();
  const deauthHandler = DeauthHandler();
  const storage = SessionStorage();

  const token = CONFIG.PRODUCTION
    ? storage.getItem("token")
    : CONFIG.DEFAULT_DEBUGGING_USER_ID.toString();
  const generateImageArray = (numberOfImages) => {
    const arrayOfImages = [];
    for (let i = 0; i < numberOfImages; i++) {
      arrayOfImages.push({ id: i, image: null });
    }
    return arrayOfImages;
  };

  const NUMBER_OF_IMAGES = 9;
  const imagesBoilerplate = generateImageArray(NUMBER_OF_IMAGES);
  const dataBoilerplate = {
    title: "",
    description: "",
    price: "",
    category: "",
    date: "",
    views: "",
  };

  const [postImages, setPostImages] = useState(imagesBoilerplate);
  const [postData, setPostData] = useState(dataBoilerplate);

  const postActivationSetter = (value) => {
    HTTP.PUT(
      `/post/active-set/${params.postId}/${value}`,
      { auth: token },
      {},
      (response, error) => {
        if (error) {
          ErrorHandler(error, setErrors, deauthHandler);
          return;
        }
        navigate(0);
      }
    );
  };

  useEffect(() => {
    HTTP.GET(
      "/post/" + params.postId,
      { auth: token },
      async (response, error) => {
        if (error) {
          ErrorHandler(error, setErrors, deauthHandler);
          return;
        }
        setPostData({
          title: response.data?.title,
          category: response.data?.category.type,
          date: response.data?.expiresAt.split("T")[0],
          views: response.data?.viewsNo,
          price: response.data?.price,
          description: response.data?.description,
          active: response.data?.active
        });

        const pendingImageFILES = response.data?.images?.map(async (image) => {
          return await Base64toFILE(image?.content, image?.type);
        });

        const ImageFILES = await Promise.all(pendingImageFILES);
        const ImageObjects = ImageFILES.map((imageFile, index) => {
          return { id: index, image: imageFile };
        });

        const numOfImages = ImageObjects.length;
        const fillArrayImages = [...imagesBoilerplate];
        fillArrayImages.splice(0, numOfImages, ...ImageObjects);

        setPostImages(fillArrayImages);
      }
    );
  }, []);

  const sendRequest = () => {
    const errorsMessages = InputValidators(postData);
    if (errorsMessages.length) {
      setErrors({
        title: "Eroare! Unul dintre campuri nu este valid!",
        message: errorsMessages,
        buttons: null,
      });
    } else {
      const dateObj = new Date(postData.date);
      const body = {
        title: postData.title,
        description: postData.description,
        price: postData.price,
        category: postData.category,
        expiresAt: dateObj.toISOString(),
        images: postImages.map((imageObj) => imageObj.image),
      };

      HTTP.formDataPUT(
        "/post/update/" + params.postId,
        { auth: token },
        body,
        (data, error) => {
          if (error) {
            ErrorHandler(error, setErrors, deauthHandler);
            return;
          }
          navigate(categoryURLs[body.category]);
        }
      );
    }
  };

  return (
    <>
      {errors && (
        <Modal
          title={errors.title}
          message={errors.message}
          buttons={errors.buttons}
          onExit={
            errors.onExit
              ? errors.onExit
              : () => {
                  setErrors(null);
                }
          }
        />
      )}
      <NewPostForm
        postImages={postImages}
        setPostImages={setPostImages}
        postData={postData}
        setPostData={setPostData}
      />
      <div className={styles.buttonContainer}>
        <TextButton
          className={styles.button}
          type={"secondary"}
          onClick={() => navigate(-1)}
          text={"Cancel"}
        />
        <TextButton
          className={styles.button}
          type={"secondary"}
          text={postData?.active ? "Deactivate Post" : "Activate Post"}
          onClick={() => postActivationSetter(!postData?.active)}
        />
        <TextButton
          className={styles.button}
          type={"primary"}
          text={"Update Post"}
          onClick={sendRequest}
        />
      </div>
    </>
  );
};

export default EditPostPage;
