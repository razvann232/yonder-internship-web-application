const InputValidators = (data) => {
  const errorArr = [];
  if (data.title.length < 3 || data.title.length > 20) {
    errorArr.push("Titlul trebuie sa aiba intre 3 si 20 de caractere!");
  }
  if (data.description.length < 10 || data.description.length > 1000) {
    errorArr.push("Descrierea trebuie sa aiba intre 10 si 1000 de caractere!");
  }
  if ((data.price == "" || parseFloat(data.price) < 0) && data.category != "EXCHANGE") {
    errorArr.push("Pretul trebuie sa aiba o valoare pozitiva!");
  }
  if (data.category == "") {
    errorArr.push("Selecteaza o categorie!");
  }
  if (data.date == "") {
    errorArr.push("Selecteaza o data!");
  }
  else{
    const date = new Date(data.date)
    if (date < new Date()) {
      errorArr.push("Selecteaza o data din viitor!");
    }
  }
  return errorArr;
};

export default InputValidators;
