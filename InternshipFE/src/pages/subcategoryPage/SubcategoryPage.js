import React, { useEffect, useState } from "react";
import { useParams, useNavigate, Link } from "react-router-dom";
import DeauthHandler from "../../components/deauthHandler/DeauthHandler";
import RequestHelper from "../../components/requestHelper/RequestHelper";
import SellAndBuyCard from "../../components/sellAndBuyCard/SellAndBuyCard";
import Modal from "../../components/modal/Modal";
import { CONFIG } from "../../config/app.config";
import SessionStorage from "../../components/sessionStorage/SessionStorage.ts";
import "./SubcategoryPage.scss";
import SearchFilterSubheader from "../../components/searchFilterSubheader/SearchFilterSubheader";
import SortingHandler from "../../components/sortingHandler/SortingHandler";
import ErrorHandler from "../../components/errorHandler/ErrorHandler";
import DateBeautifier from "../../components/dateBeautifier/DateBeautifier";
import FilteringHandler from "../../components/filteringHandler/FilteringHandler";
import SearchingHandler from "../../components/handleCategorySearch/SearchingHandler";

const SubcategoryPage = () => {
  const params = useParams();
  const navigate = useNavigate();
  const [error, setError] = useState(null);
  const deauthHandler = DeauthHandler();
  const [posts, setPosts] = useState([]);
  const [fetchedData, setFetchedData] = useState([]);
  const storage = SessionStorage();

  const checkIfPageExists = () => {
    if (!["sell", "loans", "services", "exchange"].includes(params.category)) {
      navigate("/categories");
    }
  };

  const onFilter = (e) => {
    const filteredPosts = FilteringHandler(e.label, fetchedData);
    setPosts(filteredPosts);

  };
  const onSort = (e) => {
    const sortedPosts = SortingHandler(e.value, posts);
    setPosts(sortedPosts);
  };

  const onSearch = (inputValue) => {
    const searchedPosts = SearchingHandler(inputValue, fetchedData);
    setPosts(searchedPosts);
  };

  const onCancelSearch = () => {
    setPosts(fetchedData);
  };
  

  useEffect(() => {
    checkIfPageExists();

    const token = CONFIG.PRODUCTION
      ? storage.getItem("token")
      : CONFIG.DEFAULT_DEBUGGING_USER_ID.toString();
    
    RequestHelper(CONFIG.SERVER_ADRESS).GET(
      "/post/all/" + params.category.toUpperCase(),
      { auth: token },
      (data, error) => {
        if (error) {
          ErrorHandler(error, setError, deauthHandler);
          return;
        }
        if (data.data.length === 0) {
          setError({
            title: "Oops...",
            message: "Looks like there are no posts :(",
          });
        } else {
          setPosts(data.data);
          setFetchedData(data.data);
        }
      }
    );
  }, []);

  const displayPosts = () => {
    return posts.map((post) => (
      <SellAndBuyCard
        image={
          "data:" + post.thumbnail.type + ";base64," + post.thumbnail.content
        }
        title={post.title}
        time={DateBeautifier().beautifyPostDate(post)}
        price={post.price}
        key={"postwithid_" + post.id}
        postId={post.id}
        liked={post.liked}
      />
    ));
  };

  return (
    <>
      {error && (
        <Modal
          title={error.title}
          message={error.message}
          buttons={error.buttons}
          onExit={
            error.onExit
              ? error.onExit
              : () => {
                  setError(null);
                }
          }
        />
      )}
      <SearchFilterSubheader filterOnChange={onFilter} sortOnChange={onSort} searchOnChange={onSearch} cancelSearchOnChange={onCancelSearch}/>
      <div className="main-container-subc">{displayPosts()}</div>
    </>
  );
};

export default SubcategoryPage;
