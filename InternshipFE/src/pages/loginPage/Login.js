import React, { Component } from "react";
import styles from "./login.module.scss";
import { ReactComponent as BackgroundLogo } from "../../assets/Images/Logo/bg-yonder.svg";
import { ReactComponent as LogoLarge } from "../../assets/Images/Logo/Logo_Large.svg";
import { ReactComponent as UserLogo } from "../../assets/Images/Logo/user.svg";
import { ReactComponent as LockLogo } from "../../assets/Images/Logo/lock.svg";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import SessionStorage from "../../components/sessionStorage/SessionStorage.ts";
import { emailValidator, passwordValidator, phoneValidator } from "./Validator";
import { CONFIG } from "../../config/app.config";
import RequestHelper from "../../components/requestHelper/RequestHelper";
import ErrorHandler from "../../components/errorHandler/ErrorHandler";
import DeauthHandler from "../../components/deauthHandler/DeauthHandler";
import Modal from "../../components/modal/Modal";

export function Login() {
  const [emailInput, setEmailInput] = useState("");
  const [passwordInput, setPasswordInput] = useState("");
  const navigator = useNavigate();
  const [error, setError] = useState(null);
  const deauthHandler = DeauthHandler();
  const isEmailValid = () => {
    return emailValidator.test(emailInput);
  };
  const handleClickRegister = () => {
    navigator("/register")
}
  const isPasswordValid = () => {
    if (passwordInput.length < 8) {
      return false;
    } else {
      const upperCased = passwordInput.toUpperCase();
      const lowerCased = passwordInput.toLowerCase();
      if (upperCased === passwordInput) {
        return false;
      }
      if (lowerCased === passwordInput) {
        return false;
      }
      if (!passwordValidator.test(passwordInput)) {
        return false;
      }
    }
    return true;
  };
  const areInputsValid = () => {
    return isPasswordValid() && isEmailValid();
  };

  const handleClick = (e) => {
    e.preventDefault();
    const user = { email: emailInput, password: passwordInput };
    RequestHelper(CONFIG.SERVER_ADRESS).POST(
      "/user/login",
      { auth: null },
      user,
      (data, error) => {
        console.log(data);
        if (error) {
          ErrorHandler(error, setError, deauthHandler);
          if (error.message === "Invalid password!") {
            document.getElementById("passwordInputID").className =
              "styles.textInputInvalid";
            document.getElementById("passwordInputID").value = "";
            setPasswordInput(" ");
          } else {
            document.getElementById("emailInputID").className =
              "styles.textInputInvalid";
            document.getElementById("emailInputID").value = "";
            setEmailInput(" ");
            document.getElementById("passwordInputID").className =
              "styles.textInputInvalid";
            document.getElementById("passwordInputID").value = "";
            setPasswordInput(" ");
          }
          return;
        }
        if (!data.data) {
          setError({
            title: "Oops...",
            message: "The user has a problem",
          });
          console.log("not ok");
        } else {
          SessionStorage().setItemJSON("user",data.data);
          SessionStorage().setItem("token",data.data.id);
          navigator("/categories");
        }
      }
    );
  };

  return (
    <div>
      {error && (
        <Modal
          title={error.title}
          message={error.message}
          buttons={error.buttons}
          onExit={
            error.onExit
              ? error.onExit
              : () => {
                  setError(null);
                }
          }
        />
      )}
      <BackgroundLogo className={styles.background} />
  
      <div className={styles.container}>
      <div className={styles.welcome}>WELCOME TO</div>
      <LogoLarge className={styles.ymarket} />
        <div className={styles.containerLogin}>
          <div className={styles.login}>
            <label>Login</label>
          </div>
          <div className={styles.loginForm}>
            <label className={styles.labelEmail}>
              Email address/ Phone number
            </label>
          </div>
          <div className={styles.logotext}>
            <UserLogo className={styles.userLogo} />
            <input
              className={
                emailInput.length === 0
                  ? styles.textEmail
                  : isEmailValid() || phoneValidator.test(emailInput)
                  ? styles.textInputValid
                  : styles.textInputInvalid
              }
              type="text"
              placeholder="Add Email address/ Phone number"
              onChange={(e) => {
                setEmailInput(e.target.value);
              }}
              id="emailInputID"
            ></input>
          </div>
          <div className={styles.password}>
            <label className={styles.labelPassword}>Password</label>
          </div>
          <div className={styles.passwordLogo}>
            <LockLogo className={styles.lockLogo} />
            <input
              className={
                passwordInput.length === 0
                  ? styles.textPassword
                  : isPasswordValid()
                  ? styles.textInputValid
                  : styles.textInputInvalid
              }
              type="password"
              placeholder="Add Password"
              onChange={(e) => {
                setPasswordInput(e.target.value);
              }}
              id="passwordInputID"
            ></input>
          </div>
          <div>
            <button
              className={
                !areInputsValid()
                  ? styles.buttonLoginDisable
                  : styles.buttonLoginEnable
              }
              disabled={!areInputsValid()}
              id="register_button_id"
              onClick={handleClick}
            >
              Login
            </button>
          </div>
          <div className={styles.blanc}></div>
          <div className={styles.register}>
            <label className={styles.labelRegister}>
              Don't have an account?
            </label>
            <button className={styles.buttonRegister} onClick={handleClickRegister} >Register</button>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Login;