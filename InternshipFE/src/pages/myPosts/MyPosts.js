import "./MyPosts.scss";
import { React, useEffect, useState } from "react";
import MyPostsCard from "../../components/myPostsCard/MyPostsCard";
import { useNavigate } from "react-router-dom";
import RequestHelper from "../../components/requestHelper/RequestHelper";
import { CONFIG } from "../../config/app.config";
import SessionStorage from "../../components/sessionStorage/SessionStorage.ts";

function MyPosts() {
  const [posts, setPosts] = useState([]);
  const [message, setMessage] = useState("");
  const navigate = useNavigate();
  const storage = SessionStorage();
  const token = CONFIG.PRODUCTION
    ? storage.getItem("token")
    : CONFIG.DEFAULT_DEBUGGING_USER_ID.toString();

  useEffect(() => {
    RequestHelper("http://localhost:8080").GET(
      "/post/my-posts",
      { auth: token },
      (data, error) => {
        console.log(error);
        if (error) {
          if (error.handleable) {
            if (error.code === 401) {
              navigate("/login");
            }
          } else {
            setMessage("Error connecting to the server!");
          }
        } else {
          if (data.data.length === 0) {
            setMessage("No such posts found.");
          } else {
            setPosts(data.data);
          }
        }
      }
    );
  }, []);

  const displayTime = (post) => {
    if (
      post.timeLeftDays < 0 ||
      post.timeLeftHours < 0 ||
      post.timeLeftDays < 0
    ) {
      return "Expired";
    }

    if (post.timeLeftDays === 0) {
      if (post.timeLeftHours === 0) {
        return post.timeLeftMinutes + "min";
      }
      return post.timeLeftHours + "h" + posts.timeLeftMinutes + "min";
    }
    return (
      post.timeLeftDays +
      "d" +
      post.timeLeftHours +
      "h" +
      post.timeLeftMinutes +
      "min"
    );
  };

  const determinePrice = (price) => {
    if (price === 0) return;
    return price + " RON";
  };

  const getPostsByStatus = (status) => {
    if (message !== "") {
      return <span className="no-posts-text">{message}</span>;
    }
    return posts
      .filter((post) => post.active === status)
      .map((post) => (
        <MyPostsCard
          image={
            "data:" + post.thumbnail.type + ";base64," + post.thumbnail.content
          }
          title={post.title}
          time={displayTime(post)}
          price={determinePrice(post.price)}
          isActive={post.active}
          onUpdateClick={(e) => {
            e.stopPropagation();
            navigate(`/post/edit/${post.id}`);
          }}
          onPostClick={() => {
            navigate(`/post/view/${post.id}`);
          }}
          key={`card_id_${post.id}`}
        />
      ));
  };

  return (
    <div>
      <div className="myposts-navbar">
        <span className="myposts-nav-text">My Posts</span>
        <span
          className="myfavs-nav-text"
          onClick={() => navigate("/myfavourites")}
        >
          My Favourites
        </span>
      </div>

      {getPostsByStatus(true).length ? (
        <>
          <span className="status-posts-label">Active</span>
          <div className="myposts-container">{getPostsByStatus(true)}</div>
        </>
      ) : null}
      {getPostsByStatus(false).length ? (
        <>
          <span className="status-posts-label">Inactive</span>
          <div className="myposts-container">{getPostsByStatus(false)}</div>
        </>
      ) : null}
    </div>
  );
}

export default MyPosts;
