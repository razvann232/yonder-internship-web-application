import { React, useEffect, useState } from "react";
import ArrowLeft from "../../assets/Icons/chevron-left.svg";
import ArrowRight from "../../assets/Icons/chevron-right.svg";
import "./ImageSlider.scss";

function ImageSlider({ slides }) {
  const [current, setCurrent] = useState(0);
  const length = slides.length;

  const nextSlide = () => {
    setCurrent(current === length - 1 ? 0 : current + 1);
  };

  const prevSlide = () => {
    setCurrent(current === 0 ? length - 1 : current - 1);
  };

  return (
    <div className="carousel-container">
      <img
        src={ArrowLeft}
        className="carousel-arrow-left"
        onClick={prevSlide}
      />
      <img
        src={ArrowRight}
        className="carousel-arrow-right"
        onClick={nextSlide}
      />
      
      {slides.map((item, index) => {
        return (
          <div className={index === current ? "current-slide" : "hidden-slide"}  key={`img_div_id_${item.id}`} >
            {index === current && (
              <img
                src={"data:" + item.type + ";base64," + item.content}
                alt="some-image"
                className="carousel-image"
              />
            )}
          </div>
        );
      })}
    </div>
  );
}

export default ImageSlider;
