export const Filter = (MIN, MAX, posts) => {
  return posts.filter(
    (currentPost) => currentPost.price >= MIN && currentPost.price < MAX
  );
};
