import { Filter } from "./Filter";

const FilteringHandler = (priceFilter, posts) => {
  let filteredPosts = [];
  let max, min;
  if (priceFilter.includes("MORE THAN")) {
    min = parseInt(priceFilter.substr(9));
    max = Number.MAX_SAFE_INTEGER;
  } else if (priceFilter.includes("SHOW ALL")) {
    min = 0;
    max = Number.MAX_SAFE_INTEGER;
    filteredPosts = Filter(min, max, posts);
  } else {
    let aux = priceFilter.substr(0, priceFilter.length - 4).split("-");
    min = aux[0];
    max = aux[1];
  }
  filteredPosts = Filter(min, max, posts);

  return [...filteredPosts];
};

export default FilteringHandler;
