const SearchingHandler = (searchText, posts) => {

    searchText = searchText.toLowerCase()
    const searchPosts= []
    posts.forEach((post) => {
        if (post.title.toLowerCase().includes(searchText)  ){
            searchPosts.push(post)
        }
      });
      return searchPosts;
  };

export default SearchingHandler;