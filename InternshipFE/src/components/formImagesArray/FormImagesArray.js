import React from "react";
import FormInputImage from "../formInputImage/FormInputImage";

const FormImagesArray = ({ images, setImages }) => {

  const individualSetter = (id, data) => {
    setImages([
      ...images.slice(0, id),
      { ...images[id], image: data },
      ...images.slice(id + 1),
    ]);
  };

  return (images.map((element) =>(
    <FormInputImage
    key={"imageInputPostKeyWithId"+element.id}
      image={element.image}
      setImage={(newImage) => {
        individualSetter(element.id, newImage);
      }}
    />)
  ));
};

export default FormImagesArray;
