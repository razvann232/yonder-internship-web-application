import "../../AppConstants.scss";

export const customStyles = (optionsWidth, borderColor, textColor) => {
  return {
    menu: ({ ...css }) => ({ ...css, zIndex:50, width: optionsWidth }),
    indicatorSeparator: (styles) => ({ display: "none" }),
    container: (styles, state) => ({
      ...styles,
      height: 36,
      fontSize: 16,
      fontWeight: 600,
      borderRadius: 6,
      color: textColor,
      marginRight: 20,
    }),
    control: (styles, state) => ({
      // none of react-select's styles are passed to <Control />
      ...styles,
      height: 36,
      border: state.isFocused ? ` 1px solid ${borderColor}` : ` 1px solid ${borderColor}`,
      color: textColor,
      borderRadius: 6,
      boxShadow: "none",
      "&:hover": {
        border: ` 1px solid ${borderColor}`,
        cursor: "pointer",
        border: "1px solid #92AFCC",
        color: "#92AFCC",
      },
    }),
    dropdownIndicator: () => ({
      display: "none",
    }),
    singleValue: (provided) => ({
      ...provided,
      color: textColor,
      "&:hover": {
        color: "#92AFCC",
      },
    }),
    option: (provided) => ({
      ...provided,
      "&:hover": {},
    }),
    placeholder: (provided) => ({
      ...provided,
      "&:hover": {
        color: "#92AFCC",
      },
    }),
  };
};
