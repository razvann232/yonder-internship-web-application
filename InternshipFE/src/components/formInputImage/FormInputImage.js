import { useEffect, useRef } from "react";
import React from "react";
import styles from "./FormInputImageStyle.module.scss";
import { ReactComponent as PlusIcon } from "../../assets/Icons/+.svg";
import { ReactComponent as TrashIcon } from "../../assets/Icons/trash.svg";

const FormInputImage = ({ image, setImage }) => {
  const inputFileRef = useRef(null);

  const handleImageChange = (e) => {
    if (e.target.files && e.target.files[0]) {
      setImage(e.target.files[0]);
    } else {
      setImage(null);
    }
  };
  
  const imageURL = image && URL.createObjectURL(image);
  
  const imageThumbnail =
    <React.Fragment>
      <img className={styles.image} src={imageURL} />
      <div
        className={styles.overlay}
        onClick={() => {
          inputFileRef.current.value = null;
          setImage(null);
        }}
      >
        <TrashIcon className={styles.trashIcon} />
      </div>
    </React.Fragment>

  return (
    <React.Fragment>
      <div
        onClick={() => {
          if (!image) inputFileRef.current.click();
        }}
        className={styles.container}
      >
        <input
          onChange={handleImageChange}
          type="file"
          ref={inputFileRef}
          style={{ display: "none" }}
        />
        {image ? imageThumbnail : <PlusIcon />}
      </div>
    </React.Fragment>
  );
};

export default FormInputImage;
