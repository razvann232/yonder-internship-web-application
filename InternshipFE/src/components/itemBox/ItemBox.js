import React from "react";
import "./ItemBox.scss";
import Arrow from "../../assets/Icons/arrow-narrow-right.svg";

function ItemBox({ image, newItems, title }) {
  return (
    <div className="item-box">
      <img className="item-box-image" src={image} alt="category-image"/>
      <hr className="grey-line" />
      <div className="text-container-flex">
        <span className="main-text">{title}</span>
        <img className="arrow-right" src={Arrow} alt="arrow-sign"/>
      </div>
      <span className="number-posts">{newItems} available posts</span>
    </div>
  );
}

export default ItemBox;
