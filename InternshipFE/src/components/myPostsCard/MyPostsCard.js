import { React } from "react";
import "./MyPostsCard.scss";
import "../../AppConstants.scss";

function MyPostsCard({ image, title, time, price, isActive, onUpdateClick,onPostClick }) {
  return (
      <div className="card-myposts" onClick={onPostClick}>
        <div className="card-images">
          <img className={`product-image-myposts product-image-myposts--${isActive ? 'active' : 'inactive'}`} src={image} alt="product-image"/>
        </div>
        <span className={`card-title-myp card-title-myp--${isActive ? 'active' : 'inactive'}`}>{title}</span>
        <div className="time-price-myp">
          <img className={`timer-icon-myposts--${isActive ? 'active' : 'inactive'}`} alt="timer-image"/>
          <span className={`time-myp time-myp--${isActive ? 'active' : 'inactive'}`}>{time}</span>
          <span className={`price price--${isActive ? 'active' : 'inactive'}`}>{price ? price : ''}</span>
        </div>
        <button className="update-button" onClick={onUpdateClick}>{isActive ? "Update Post" : "Activate Post"}</button>
      </div>
  );
}

export default MyPostsCard;
