import React, { useState } from "react";
import "./SellAndBuyCard.scss";
import TimerPic from "../../assets/Icons/timer.svg";
import LikePicNoFill from "../../assets/Icons/like-white.svg";
import LikePicFilled from "../../assets/Icons/like-filled-white.svg";
import { useNavigate } from "react-router-dom";
import RequestHelper from "../../components/requestHelper/RequestHelper";
import {CONFIG} from "../../config/app.config"
import SessionStorage from "../sessionStorage/SessionStorage.ts"

function SellAndBuyCard({ image, title, time, price, postId, liked }) {
  const [likePic, setLikePic] = useState(liked ? LikePicFilled : LikePicNoFill);
  const [message, setMessage] = useState("");
  const navigate = useNavigate();
  const storage = SessionStorage();
  const token = CONFIG.PRODUCTION
  ? storage.getItem("token")
  : CONFIG.DEFAULT_DEBUGGING_USER_ID.toString();

  const registerLike = () => {
    RequestHelper("http://localhost:8080").POST(
      "/post/like/" + postId,
      { auth: token },
      {},
      (data, error) => {
        if (error) {
          if (error.handleable) {
            if (error.code === 401) {
              navigate("/login");
            }
          } else {
            setMessage("Error connecting to the server!");
          }
        }
      }
    );
  };

  const unregisterLike = () => {
    RequestHelper("http://localhost:8080").DELETE(
      "/post/unlike/" + postId,
      { auth: token },
      (data, error) => {
        if (error) {
          if (error.handleable) {
            if (error.code === 401) {
              navigate("/login");
            }
          } else {
            setMessage("Error connecting to the server!");
          }
        }
      }
    );
  };

  const fillLike = (e) => {
    e.stopPropagation()
    if (likePic === LikePicNoFill) {
      setLikePic(LikePicFilled);
      registerLike();
    } else {
      setLikePic(LikePicNoFill);
      unregisterLike();
    }
  };
  const displayCard = () => {
    if (message !== "") {
      return <span className="error-text">{message}</span>;
    }

    return (
      <div onClick={() => {navigate("/post/view/" + postId)}}>
        <div className="card-images">
          <img className="product-image" src={image} alt="product-image" ></img>
          <img className="like-icon-card" src={likePic} onClick={fillLike} alt="heart-sign" />
        </div>
        <span className="card-title-sab">{title}</span>
        <div className="time-price-sab">
          <img src={TimerPic} alt="timer-sign" />
          <span className="time-sab">{time}</span>
          <span className="price-sab">{price ? price+" RON" : ""}</span>
        </div>
      </div>
    );
  };

  return (
    <div className="card">
      {displayCard()}
    </div>
  );
}

export default SellAndBuyCard;
