import React from "react";
import styles from "./SingleNotification.module.scss";
import DateBeautifier from "../dateBeautifier/DateBeautifier";

const SingleNotification = ({ data, onView }) => {
  const imageURL =
    "data:" + data?.thumbnail?.type + ";base64," + data?.thumbnail?.content;

  return (
    <div className={styles.notificationBody}>
      <img src={imageURL} className={styles.image} />
      <div className={styles.content}>
        <h1>{data?.content}</h1>
        <div className={styles.footer}>
          <span className={styles.time}>
            {DateBeautifier().timeSince(data?.date)} ago
          </span>
          <span className={styles.view} onClick={onView}>View</span>
        </div>
      </div>
    </div>
  );
};

export default SingleNotification;
