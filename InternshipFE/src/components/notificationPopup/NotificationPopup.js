import React, { useState } from "react";
import RequestHelper from "../requestHelper/RequestHelper";
import styles from "./NotificationPopup.module.scss";
import SingleNotification from "./SingleNotification";
import { CONFIG } from "../../config/app.config";
import SessionStorage from "../../components/sessionStorage/SessionStorage.ts";
import ErrorHandler from "../errorHandler/ErrorHandler";
import Modal from "../modal/Modal";
import DeauthHandler from "../deauthHandler/DeauthHandler";
import { useEffect } from "react";

const NotificationPopup = ({
  children,
  className,
  notifications = [],
  setNotifications,
  ...props
}) => {
  const [showNotifications, setShowNotifications] = useState(false);
  const HTTP = RequestHelper(CONFIG.SERVER_ADRESS);
  const storage = SessionStorage();
  const [error, setError] = useState(null);
  const deauthHandler = DeauthHandler();
  const token = CONFIG.PRODUCTION
    ? storage.getItem("token")
    : CONFIG.DEFAULT_DEBUGGING_USER_ID.toString();

  const viewNotification = (id) => {
    HTTP.PUT(
      `/notification/seen/${id}`,
      { auth: token },
      {},
      (response, error) => {
        if (error) {
          ErrorHandler(error, setError, deauthHandler);
          return;
        }
        const newNotificationArray = notifications.filter(
          (notification) => notification.id != id
        );
        setNotifications(newNotificationArray);
      }
    );
  };

  const handleClick = () => {
    setShowNotifications(!showNotifications);
  };

  const notificationList = notifications.length
    ? notifications
        .sort((a, b) => new Date(b?.date) - new Date(a?.date))
        .map((notification) => {
          return (
            <SingleNotification
              key={`notification_with_id_${notification.id}`}
              data={notification}
              onView={() => {
                viewNotification(notification?.id);
              }}
            />
          );
        })
    : [];

  const noNotificationsPlaceHolder = (
    <div className={styles.placeholder}>You have no notifications...</div>
  );

  useEffect(() => {
    if (!notifications.length) {
      setShowNotifications(false);
    }
  }, [notifications]);

  return (
    <>
      {error && (
        <Modal
          title={error.title}
          message={error.message}
          buttons={error.buttons}
          onExit={
            error.onExit
              ? error.onExit
              : () => {
                  setError(null);
                }
          }
        />
      )}
      <div className={`${className} ${styles.container}`} {...props}>
        <span onClick={handleClick}>{children}</span>
        {showNotifications && (
          <div className={styles.popup}>
            {notificationList.length
              ? notificationList
              : noNotificationsPlaceHolder}
          </div>
        )}
      </div>
    </>
  );
};

export default NotificationPopup;
