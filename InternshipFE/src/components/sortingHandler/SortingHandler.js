import { SORTING_OPTIONS } from "../../config/subheaderConstants";

const SortingHandler = (sortingType, data) => {
  let comparator = (a, b) => a?.price - b?.price;

  SORTING_OPTIONS.forEach((option) => {
    if (option.value == sortingType) comparator = option.comparator;
  });
  
  const sortedArr = data?.sort(comparator);
  return [...sortedArr];
};

export default SortingHandler;
