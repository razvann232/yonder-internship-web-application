import React from "react";
import "./Comment.scss";
import FemalePic from "../../assets/Images/JPG/female.jpg";

function Comment({data}) {
  return (
    <div className="comment-container">
      <img src={data.user?.profilePicture === null ? FemalePic : "data:" + data.user?.profilePicture?.type + ";base64," + data.user?.profilePicture?.content} className="comment-picture" alt="user-picture" />
      <div className="user-info">
        <span className="user-name-text">{data.user?.name}</span>
        <span className="date-text">{data.date?.substring(0,10)}</span>
      </div>
      <span className="comment-text">
        {data.content}
      </span>
    </div>
  );
}

export default Comment;
