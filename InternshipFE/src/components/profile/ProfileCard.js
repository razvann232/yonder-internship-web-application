import React, { useRef } from "react";
import InputForm from "./InputForm";
import UserPicture from "./UserPicture";
import "./ProfileCard.scss";
import { useState, useEffect } from "react";
import { useForm, FormProvider } from "react-hook-form";
import { ReactComponent as SeePasswordIcon } from "../../assets/Icons/eye.svg";
import RequestHelper from "../../components/requestHelper/RequestHelper";
import SessionStorage from "../../components/sessionStorage/SessionStorage.ts";
import {CONFIG} from "../../config/app.config"

const ProfileCard = () => {
  const [hidden, setHidden] = useState(false);
  const [hiddenChangePass, sethiddenChangePass] = useState(false);
  const [newPasswordShown, setNewPasswordShown] = useState(false);
  const [confirmPasswordShown, setConfirmPasswordShown] = useState(false);
  const [passwordShown, setPasswordShown] = useState(false);
  const [changePassValue, setChangePassValue] = useState("Change Password");

  const inputRef = useRef(null);

  const [user, setUser] = useState(SessionStorage().getItemJSON("user"));
  const [image, setImage] = useState(user?.profilePicture);
  const [imagePreview, setImagePreview] = useState(false);

  const token = CONFIG.PRODUCTION
    ? SessionStorage().getItem("token")
    : CONFIG.DEFAULT_DEBUGGING_USER_ID.toString();

  const [formData, setFormData] = useState({
    username: user.name,
    email: user.email,
    phoneNumber: user.phoneNumber,
    gender: user.gender,
  });

  const methods = useForm({
    mode: "onBlur",
    reValidateMode: "onChange",
    defaultValues: formData,
    criteriaMode: "firstError",
    shouldFocusError: true,
    shouldUnregister: false,
    shouldUseNativeValidation: false,
  });

  const [pass, setPass] = useState("");
  const [confPass, setConfPass] = useState("");

  useEffect(() => {
    const [newPassword, confirmPassword] = methods.getValues([
      "newPassword",
      "confirmPassword",
    ]);

    if (newPassword !== confirmPassword) {
      methods.setError("confirmPassword", {
        message: "Password did not match!",
      });
    }
  }, [pass, confPass]);

  useEffect(() => {
    if (hiddenChangePass) {
      methods.trigger("password", { shouldFocus: true });
    } else {
      methods.unregister("password");
      methods.unregister("confirmPassword");
      methods.unregister("newPassword");
    }
  }, [hiddenChangePass]);

  const checkForm = () => {
    return !!Object.keys(methods.formState.errors).length;
  };

  const toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });

  const submitChanges = () => {
    const HTTP = RequestHelper("http://localhost:8080");

    const body = {
      name: methods.getValues().username,
      email: methods.getValues().email,
      phoneNumber: methods.getValues().phoneNumber,
      gender: methods.getValues().gender,
      password: methods.getValues().newPassword,
    };

    HTTP.PUT("/user/update", { auth: token }, body, (data, error) => {
      if (error) {
        console.log("eroare: ", error);
      } else {
        SessionStorage().setItemJSON("user", data.data);
        setHidden(false);
      }
    });

    imagePreview &&
      HTTP.formDataPOST(
        "/user/profile-picture/save",
        { auth: token },
        { image: image },
        (data, error) => {
          if (error) {
            console.log("eroare: ", error);
          } else {
            SessionStorage().setItemJSON("user", data.data);
            setImage(data.data?.profilePicture);
            setImagePreview(false);
            setHidden(false);
          }
        }
      );
  };

  const onFileUpload = async (e) => {
    if (e.target.files && e.target.files[0]) {
      const file = e.target.files[0];
      try {
        const result = await toBase64(file);
        setImagePreview({
          type: file.type,
          content: result.split("base64,")[1],
        });
        setImage(file);
      } catch (error) {
        console.error(error);
      }
    } else {
      setImage(null);
    }
  };

  const onDelete = () => {
    const HTTP = RequestHelper("http://localhost:8080");
    HTTP.DELETE(
      "/user/profile-picture/delete",
      { auth: token },
      (data, error) => {
        if (error) {
          console.log("eroare: ", error);
        } else {
          SessionStorage().setItemJSON("user", data.data);
          setImage(data.data?.profilePicture);
        }
      }
    );
  };

  const onPressChangePass = () => {
    sethiddenChangePass(!hiddenChangePass);
    !hiddenChangePass
      ? setChangePassValue("Cancel")
      : setChangePassValue("Change Password");
  };

  return (
    <div className="profile-container-with-buttons">
      <div className="profile-container">
        <div className="user-picture">
          <UserPicture userImage={imagePreview ? imagePreview : image} />
          {!hidden && (
            <div>
              <label className="label-name">{formData.username}</label>
            </div>
          )}
          {hidden && (
            <div className="button-change-picture">
              <label htmlFor="filePicker">Change profile picture</label>
              <input
                id="filePicker"
                ref={inputRef}
                style={{ visibility: "hidden" }}
                type={"file"}
                onChange={onFileUpload}
              ></input>
            </div>
          )}
          {hidden && (
            <div>
              <button className="button-remove-picture" onClick={onDelete}>
                Remove Profile Picture
              </button>
            </div>
          )}
        </div>

        <div className="line-between-elements"></div>

        <div className="form-container">
          <FormProvider {...methods}>
            <form className="form-profile">
              <InputForm submitForm={submitChanges} disableForm={!hidden} />
            </form>
          </FormProvider>
          {hidden && (
            <div className="container-button-change-pass">
              <button
                onClick={onPressChangePass}
                className="button-change-password"
              >
                {changePassValue}
              </button>
            </div>
          )}

          {hiddenChangePass && (
            <div className="form-change-password">
              <div className="container-input-pass">
                <label>Current Password</label>
                <div className="rectangle-pass">
                  <input
                    {...methods.register("password", {
                      required: true,
                      pattern:
                        /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})/i,
                    })}
                    type={passwordShown ? "text" : "password"}
                    placeholder="Password"
                  />

                  <div className="container-input-icon-pass">
                    <SeePasswordIcon
                      className="input-icon-pass"
                      id="seePass"
                      onClick={() => setPasswordShown(!passwordShown)}
                      disabled={passwordShown}
                    />
                  </div>
                </div>
                {methods.formState.errors?.password?.type === "pattern" && (
                  <p className="error-message">
                    Your password must have at least 8 characters, at least one
                    lowercase and uppercase letter, one digit and one special
                    character
                  </p>
                )}
              </div>

              <div className="container-input-pass">
                <label>New Password</label>
                <div className="rectangle-pass">
                  <input
                    {...methods.register("newPassword", {
                      onChange: (e) => setPass(e),
                      required: true,
                      pattern:
                        /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})/i,
                    })}
                    type={newPasswordShown ? "text" : "password"}
                    placeholder="Add New Password"
                  />
                  <div className="container-input-icon-pass">
                    <SeePasswordIcon
                      className="input-icon-pass"
                      id="seePass"
                      onClick={() => setNewPasswordShown(!newPasswordShown)}
                    />
                  </div>
                </div>
                {(methods.formState.errors?.newPassword?.type === "required" ||
                  methods.formState.errors?.newPassword?.type ===
                    "pattern") && (
                  <p className="error-message">
                    Your password must have at least 8 characters, at least one
                    lowercase and uppercase letter, one digit and one special
                    character
                  </p>
                )}
              </div>

              <div className="container-input-pass">
                <label>Confirm Password</label>
                <div className="rectangle-pass">
                  <input
                    {...methods.register("confirmPassword", {
                      onChange: (e) => setConfPass(e),
                    })}
                    type={confirmPasswordShown ? "text" : "password"}
                    placeholder="Confirm Password"
                  />
                  <div className="container-input-icon-pass">
                    <SeePasswordIcon
                      className="input-icon-pass"
                      id="seePass"
                      onClick={() =>
                        setConfirmPasswordShown(!confirmPasswordShown)
                      }
                    />
                  </div>
                </div>
                {methods.formState.errors?.confirmPassword && (
                  <p className="error-message">
                    {methods.formState.errors?.confirmPassword?.message}
                  </p>
                )}
              </div>
            </div>
          )}
        </div>
      </div>
      {!hidden && (
        <button onClick={() => setHidden(!hidden)} className="button-edit">
          Edit
        </button>
      )}

      {hidden && (
        <div className="buttons-save-cancel">
          <button
            className="button-cancel"
            disabled={hiddenChangePass}
            onClick={() => setHidden(!hidden)}
          >
            Cancel
          </button>
          <button
            onClick={submitChanges}
            disabled={checkForm()}
            className="button-save-changes"
          >
            Save
          </button>
        </div>
      )}
    </div>
  );
};

export default ProfileCard;
