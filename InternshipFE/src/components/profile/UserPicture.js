import React from "react";
import "./UserPicture.scss";
import DefaultPicture from "../../../src/assets/Images/JPG/defaultPictureUser.jpg";

const UserPicture = ({ userImage }) => {
  const imageLink = userImage?.content
    ? "data:" + userImage?.type + ";base64," + userImage?.content
    : null;
  return (
    <div className="container-picture-name">
      {imageLink ? (
        imageLink && (
          <img
            src={imageLink}
            alt="user picture not found"
            className="profile-image"
          />
        )
      ) : (
        <img
          src={DefaultPicture}
          alt="default picture"
          className="profile-image"
        />
      )}
    </div>
  );
};

export default UserPicture;
