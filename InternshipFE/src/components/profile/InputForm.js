import React, { Fragment } from "react";
import "./InputForm.scss";
import { ReactComponent as DeleteDetailsIcon } from "../../assets/Icons/x.svg";
import { ReactComponent as SeePasswordIcon } from "../../assets/Icons/eye.svg";
import { ReactComponent as ChooseGenderIcon } from "../../assets/Icons/caret-down filled.svg";
import { useFormContext } from "react-hook-form";
import { useState } from "react";

const InputForm = ({ submitForm, disableForm }) => {
  const {
    register,
    setValue,
    trigger,
    formState: { errors },
  } = useFormContext();
  const handleChange = (event) => {};

  const handleChangePhone = (event) => {};

  const deleteEmailValue = () => {
    if (!disableForm) {
      setValue("email", "");
      trigger("email", { shouldFocus: true });
    }
  };

  const deletePhoneValue = () => {
    if (!disableForm) {
      setValue("phoneNumber", "");
      trigger("phoneNumber", { shouldFocus: true });
    }
  };

  return (
    <Fragment>
      <div className="container-input-info">
        <label>Name</label>
        <div className="rectangle">
          <input
            {...register("username", { required: true, minLength: 3 })}
            placeholder="Username"
            disabled={true}
          />
        </div>
        {(errors.username?.type === "required" ||
          errors.username?.type === "minLength") && (
          <p className="error-message">
            Username must have minimum 3 characters
          </p>
        )}
      </div>

      <div className="container-input-info">
        <label>Email</label>
        <div className="rectangle">
          <input
            {...register("email", {
              required: true,
              pattern: /[a-z0-9]+@[a-z]+\.[a-z]{2,3}/i,
            })}
            placeholder="Email"
            onChange={(e) => handleChange(e)}
            disabled={disableForm}
          />

          <div className="container-input-icon">
            <DeleteDetailsIcon
              className="input-icon"
              onClick={deleteEmailValue}
              disabled={disableForm}
            ></DeleteDetailsIcon>
          </div>
        </div>
        {(errors.email?.type === "required" ||
          errors.email?.type === "pattern") && (
          <p className="error-message">Email is not valid</p>
        )}
      </div>

      <div className="container-input-info">
        <label>Phone</label>
        <div className="rectangle">
          <input
            {...register("phoneNumber", {
              required: true,
              pattern: /07[0-9]{8}/i,
            })}
            placeholder="Phone number"
            onChange={handleChangePhone}
            disabled={disableForm}
          />

          <div className="container-input-icon">
            <DeleteDetailsIcon
              className="input-icon"
              onClick={deletePhoneValue}
              disabled={disableForm}
            />
          </div>
        </div>
        {(errors.phoneNumber?.type === "required" ||
          errors.phoneNumber?.type === "pattern") && (
          <p className="error-message">
            Phone number is not valid. Eg. 07.... (10 characters)
          </p>
        )}
      </div>

      <div className="container-input-info">
        <label>Gender</label>
        <div className="rectangle">
          <select
            defaultValue=""
            {...register("gender", { required: true })}
            disabled={disableForm}
          >
            <option value="" disabled>
              Select gender
            </option>
            <option value="FEMALE">female</option>
            <option value="MALE">male</option>
            <option value="OTHER">other</option>
          </select>
        </div>
        {errors.gender?.type === "required" && (
          <p className="error-message">You have to choose a gender</p>
        )}
      </div>
    </Fragment>
  );
};

export default InputForm;
