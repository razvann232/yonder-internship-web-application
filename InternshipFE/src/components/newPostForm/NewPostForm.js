import { React, useEffect, useRef, useState } from "react";
import Dropdown from "../dropdown/Dropdown";
import styles from "./NewPostFormStyle.module.scss";
import FormImagesArray from "../formImagesArray/FormImagesArray";

const categories = [
  { value: "SELL", label: "Sell & Buy" },
  { value: "EXCHANGE", label: "Exchange" },
  { value: "LOANS", label: "Loan" },
  { value: "SERVICES", label: "Service" },
];

const NewPostForm = ({ postData, setPostData, postImages, setPostImages }) => {
  const [showPrice, setshowPrice] = useState(true);

  useEffect(() => {
    if(!showPrice)
      setPostData({...postData, price:0})
  }, [showPrice])
  

  return (
    <div className={styles.container}>
      <div className={styles.leftChild}>
        <label>Add Images</label>
        <div className={styles.imageGrid}>
          <FormImagesArray images={postImages} setImages={setPostImages} />
        </div>
      </div>
      <div className={styles.spacer}></div>
      <div className={styles.rightChild}>
        <div className={styles.inputGroup}>
          <label>Title</label>
          <br />
          <input
            value={postData?.title}
            onChange={(e) =>
              setPostData({ ...postData, title: e.target.value })
            }
            className={styles.input}
          ></input>
        </div>
        <div className={styles.inputGroup}>
          <label>Description</label>
          <br />
          <textarea
            value={postData?.description}
            onChange={(e) =>
              setPostData({ ...postData, description: e.target.value })
            }
          ></textarea>
        </div>
        {showPrice && (
          <div className={styles.inputGroup}>
            <label>Price</label>
            <br />
            <input
              type="number"
              min={0}
              value={postData?.price}
              onChange={(e) =>
                setPostData({ ...postData, price: e.target.value })
              }
              className={styles.input}
            ></input>
          </div>
        )}
        <div className={styles.flex}>
          <div className={styles.bottomInputs}>
            <label>Category</label>
            <Dropdown
              value={categories.filter((e) => e.value == postData?.category)}
              onChange={(option) => {
                if (option.value == "EXCHANGE") {
                  setshowPrice(false)
                }
                else{
                  setshowPrice(true)
                }
                setPostData({ ...postData, category: option.value });
              }}
              menuPlacement="top"
              borderColor="#8C8C8C"
              textColor="black"
              optionsWidth={180}
              className={styles.dropdown}
              text={"Select a category..."}
              options={categories}
            />
          </div>
          {postData?.views && (
            <div className={styles.bottomInputs}>
              <label>Views</label>
              <h1>{postData.views}</h1>
            </div>
          )}
          <div className={styles.bottomInputs}>
            <label>Expires On</label>
            <br />
            <input
              value={postData?.date}
              onChange={(e) =>
                setPostData({ ...postData, date: e.target.value })
              }
              type="date"
              className={styles.dateSelector}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewPostForm;
