import React from "react";
import TextButton from "../textButton/TextButton";
import styles from "./SearchFilterSubheader.module.scss";
import { useNavigate } from "react-router-dom";
import { ReactComponent as PlusIcon } from "../../assets/Icons/+.svg";
import { ReactComponent as FilterIcon } from "../../assets/Icons/filter.svg";
import { ReactComponent as SortIcon } from "../../assets/Icons/arrows-down-up.svg";
import Dropdown from "../dropdown/Dropdown";
import { SUBHEADER_CONSTANTS, SORTING_OPTIONS } from "../../config/subheaderConstants";
import SearchField from "../searchField/SearchField";

const filterOptions = [
  { value: 1, label: "0 - 50 RON" },
  { value: 2, label: "50-100 RON" },
  { value: 3, label: "100-200 RON" },
  { value: 4, label: "200-500 RON" },
  { value: 5, label: "MORE THAN 500" },
  { value: 6, label: "SHOW ALL"}
];

const SearchFilterSubheader = ({ filterOnChange, sortOnChange, searchOnChange, cancelSearchOnChange }) => {
  const navigate = useNavigate();
  return (
    <div className={styles.bar}>
      <div className={styles.flexCenter}>
        <TextButton
          className={styles.left}
          Icon={PlusIcon}
          text={"Add New Item"}
          onClick={() => navigate("/newpost")}
        />
      </div>
      <div className={styles.flexCenter}>
        <Dropdown
          onChange={sortOnChange}
          options={SORTING_OPTIONS}
          LeftIcon={SortIcon}
          text={SUBHEADER_CONSTANTS.sortPlaceholder}
        />
        <Dropdown
          onChange={filterOnChange}
          options={filterOptions}
          LeftIcon={FilterIcon}
          text={SUBHEADER_CONSTANTS.filterPlaceholder}
        />
        <div className={styles.spacer}></div>
        <SearchField className={styles.right} onSearch={searchOnChange} onCancel={cancelSearchOnChange}/>
      </div>
    </div>
  );
};

export default SearchFilterSubheader;