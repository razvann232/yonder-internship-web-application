import React from "react";
import styles from "./Register.module.scss";
import { useNavigate } from "react-router-dom";

function AlreadyHaveAccount() {
  const navigator = useNavigate();
  return (
    <div>
      <label className={styles.already_have_account_label}>
        Already have an account?
      </label>
      <button
        className={styles.login_button}
        onClick={() => navigator("/login")}
        id="login-button-id"
      >
        Login
      </button>
    </div>
  );
}

export default AlreadyHaveAccount;
