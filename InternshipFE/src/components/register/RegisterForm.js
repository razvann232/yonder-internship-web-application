import React from "react";
import { ReactComponent as UserImg } from "../../assets/Icons/user.svg";
import { ReactComponent as MailImg } from "../../assets/Icons/mail.svg";
import { ReactComponent as PhoneImg } from "../../assets/Icons/phone.svg";
import { ReactComponent as LockImg } from "../../assets/Icons/lock.svg";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  nameValidator,
  emailValidator,
  phoneValidator,
  passwordValidator,
} from "./Validator";
import styles from "./Register.module.scss";
import RegisterPopUp from "./RegisterPopUp";
import SessionStorage from "../sessionStorage/SessionStorage.ts";
import ErrorHandler from "../errorHandler/ErrorHandler";
import Modal from "../modal/Modal";
import DeauthHandler from "../deauthHandler/DeauthHandler";
import RequestHelper from "../requestHelper/RequestHelper";

function RegisterForm() {
  const [nameInput, setNameInput] = useState("");
  const [emailInput, setEmailInput] = useState("");
  const [passwordInput, setPasswordInput] = useState("");
  const [phoneInput, setPhoneInput] = useState("");
  const [stringResp, setStringResp] = useState("");
  const [flag, setFlag] = useState(false);
  const deauthHandler = DeauthHandler();
  const [error, setError] = useState(null);

  const navigator = useNavigate();

  function SendInfo() {
    console.log(passwordInput)
    const user = {
      name: nameInput,
      password: passwordInput,
      email: emailInput,
      phoneNumber: phoneInput,
    };
    RequestHelper("http://localhost:8080").POST(
      "/user/save",
      { auth: null },
      user,
      (data, error) => {
        if (error) {
          ErrorHandler(error, setError, deauthHandler);
          document.getElementById("emailInputID").value = "";
          setEmailInput(" ");
          document.getElementById("emailInputID").className =
            "styles.bad_input_email";
          setStringResp(error.message);
          setFlag(true);
        } else {
          SessionStorage().setItemJSON("user",data.data);
          SessionStorage().setItemJSON("token",data.data.id);
          navigator("/categories");
        }
      }
    );
  }

  const isEmailValid = () => {
    return emailValidator.test(emailInput) && emailInput.length > 10;
  };

  const isNameValid = () => {
    return (
      nameInput.length > 3 &&
      nameInput.charAt(0) !== " " &&
      nameInput.charAt(0) === nameInput.charAt(0).toUpperCase() &&
      nameValidator.test(nameInput)
    );
  };

  const isPasswordValid = () => {
    if (passwordInput.length < 8) {
      return false;
    } else {
      const upperCased = passwordInput.toUpperCase();
      const lowerCased = passwordInput.toLowerCase();
      if (upperCased === passwordInput) {
        return false;
      }
      if (lowerCased === passwordInput) {
        return false;
      }
      if (!passwordValidator.test(passwordInput)) {
        return false;
      }
    }
    return true;
  };

  const isPhoneValid = () => {
    return phoneValidator.test(phoneInput);
  };

  const areInputsValid = () => {
    return (
      isNameValid() &&
      isEmailValid() &&
      isPasswordValid() &&
      isPhoneValid() &&
      nameInput &&
      emailInput &&
      passwordInput &&
      phoneInput
    );
  };

  return (
    <div className={styles.div_form}>
      {error && (
        <Modal
          title={error.title}
          message={error.message}
          buttons={error.buttons}
          onExit={
            error.onExit
              ? error.onExit
              : () => {
                  setError(null);
                }
          }
        />
      )}
      <label className={styles.register_label}>Register</label>
      <div className={styles.form}>
        <div>
          <label>Name</label>
          <div>
            <input
              className={
                nameInput.length === 0
                  ? styles.input
                  : isNameValid()
                  ? styles.proper_input
                  : styles.bad_input
              }
              placeholder="Add Name"
              onChange={(e) => {
                setNameInput(e.target.value);
              }}
              id="nameInputID"
              required
            ></input>
            <UserImg className={styles.icon} />
          </div>
        </div>

        <div>
          <label>Email Address</label>
          <div>
            <input
              className={
                emailInput.length === 0
                  ? styles.input
                  : isEmailValid()
                  ? styles.proper_input
                  : styles.bad_input
              }
              placeholder="Add Email Address"
              onChange={(e) => {
                setEmailInput(e.target.value);
              }}
              id="emailInputID"
              required
            ></input>
            <MailImg className={styles.icon} />
          </div>
        </div>

        <div>
          <label>Phone Number</label>
          <div>
            <input
              className={
                phoneInput.length === 0
                  ? styles.input
                  : isPhoneValid()
                  ? styles.proper_input
                  : styles.bad_input
              }
              placeholder="Add Phone Number"
              onChange={(e) => {
                setPhoneInput(e.target.value);
              }}
              id="phoneInputID"
              required
            ></input>
            <PhoneImg className={styles.icon} />
          </div>
        </div>

        <div>
          <label>Password</label>
          <div>
            <input
              type="password"
              className={
                passwordInput.length === 0
                  ? styles.input
                  : isPasswordValid()
                  ? styles.proper_input
                  : styles.bad_input
              }
              placeholder="Add Password"
              onChange={(e) => {
                setPasswordInput(e.target.value);
              }}
              id="passwordInputID"
              required
            ></input>
            <LockImg className={styles.icon} />
          </div>
          <RegisterPopUp
            trigger={false}
            message={stringResp}
            setFlag={setFlag}
            flag={flag}
          />

          <button
            className={
              !areInputsValid()
                ? styles.register_button_empty
                : styles.register_button_filled
            }
            disabled={!areInputsValid()}
            onClick={() => {
              SendInfo();
            }}
            id="register_button_id"
          >
            Register
          </button>
        </div>
      </div>
    </div>
  );
}

export default RegisterForm;
