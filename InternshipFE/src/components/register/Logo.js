import React from "react";
import { ReactComponent as YonderLogo } from "../../assets/Images/Logo_Large.svg";
import styles from "./Register.module.scss";

function Logo() {
  return (
    <div>
      <label className={styles.welcome_message}>WELCOME TO</label>
      <YonderLogo />
    </div>
  );
}

export default Logo;
