import React from "react";

import { ReactComponent as BackgroundLogo } from "../../assets/Images/Background_image_Login_Register screen.svg";

import Logo from "./Logo";
import RegisterForm from "./RegisterForm";
import styles from "./Register.module.scss";
import AlreadyHaveAccount from "./AlreadyHaveAccount";

function Register() {
  return (
    <div>
      <BackgroundLogo className={styles.background} />
      <div className={styles.main_container}>
        <Logo />
        <RegisterForm />
        <div className={styles.line}></div>
        <AlreadyHaveAccount />
      </div>
    </div>
  );
}

export default Register;
