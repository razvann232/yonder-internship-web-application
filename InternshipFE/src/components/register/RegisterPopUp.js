import React from "react";
import styles from "./Register.module.scss";

function RegisterPopUp(props) {
  return props.trigger ? (
    <div className={styles.modal}>
      <p>
        <strong>{props.message}</strong>
      </p>
      <button onClick={() => props.setFlag(!props.flag)}>OK</button>
    </div>
  ) : (
    ""
  );
}

export default RegisterPopUp;
