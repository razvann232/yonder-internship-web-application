import React, { useState } from "react";
import styles from "./LeftNavbar.module.scss";
import LeftNavbarButton from "./leftNavbarButton/LeftNavbarButton";
import { ReactComponent as CategoriesIcon } from "../../assets/Icons/layout-grid.svg";
import { ReactComponent as MyPostsIcon } from "../../assets/Icons/like.svg";
import { ReactComponent as ProfileIcon } from "../../assets/Icons/user.svg";
import { ReactComponent as SettingsIcon } from "../../assets/Icons/settings.svg";
import { ReactComponent as InfoIcon } from "../../assets/Icons/info-circle.svg";
import { ReactComponent as CollapseIcon } from "../../assets/Icons/arrow-bar-to-left.svg";
import { ReactComponent as ExpandIcon } from "../../assets/Icons/arrow-bar-to-right.svg";
import { ReactComponent as YonderLogoExpanded } from "../../assets/Images/Logo_Small_Expanded.svg";
import { ReactComponent as YonderLogoCollapsed } from "../../assets/Images/Logo_Small_Collapsed.svg";
import { useNavigate } from "react-router-dom";

const LeftNavbar = ({ collapsed, setCollapsed }) => {
  const [selectedButton, setSelectedButton] = useState(0);
  const naviagtor = useNavigate();

  const redirectIdUrlPairs = [
    {
      id: 0,
      url: "/categories",
    },
    {
      id: 1,
      url: "/myposts",
    },
    {
      id: 2,
      url: "/profile",
    },
    {
      id: 3,
      url: "/settings",
    },
    {
      id: 4,
      url: "https://tss-yonder.com/",
    },
    
  ];

  const containerStyle = collapsed
    ? `${styles.container} ${styles.collapsed}`
    : styles.container;

  const logo = collapsed ? (
    <YonderLogoCollapsed className={styles.logoCollapsed} />
  ) : (
    <YonderLogoExpanded className={styles.logo} />
  );

  const chevronButton = collapsed ? (
    <ExpandIcon
      className={`${styles.resizeIcon} ${styles.expand}`}
      onClick={() => {
        setCollapsed(!collapsed);
      }}
    />
  ) : (
    <CollapseIcon
      className={styles.resizeIcon}
      onClick={() => {
        setCollapsed(!collapsed);
      }}
    />
  );

  const handleButtonClick = (id) => {
    setSelectedButton(id);
    const pair = redirectIdUrlPairs.find((pair) => pair.id === id);
    naviagtor(pair.url);
  };

  return (
    <div className={containerStyle}>
      <div>
        {logo}
        <LeftNavbarButton
          selected={selectedButton === 0}
          collapsed={collapsed}
          Icon={CategoriesIcon}
          text={"Categories"}
          onClick={() => handleButtonClick(0)}
        />
        <LeftNavbarButton
          selected={selectedButton === 1}
          collapsed={collapsed}
          Icon={MyPostsIcon}
          text={"My Posts"}
          onClick={() => handleButtonClick(1)}
        />
        <LeftNavbarButton
          selected={selectedButton === 2}
          collapsed={collapsed}
          Icon={ProfileIcon}
          text={"Profile"}
          onClick={() => handleButtonClick(2)}
        />
      </div>
      <div>
        <LeftNavbarButton
          selected={selectedButton === 3}
          collapsed={collapsed}
          Icon={SettingsIcon}
          text={"Settings"}
          onClick={() => handleButtonClick(3)}
        />
        <LeftNavbarButton
          collapsed={collapsed}
          Icon={InfoIcon}
          text={"Info"}
          selected={selectedButton === 4}
          onClick={() =>{ window.location.href = "https://tss-yonder.com/"}}
        />
        {chevronButton}
      </div>
    </div>
  );
};

export default LeftNavbar;