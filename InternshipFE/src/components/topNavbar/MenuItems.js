export const navBarItems = [
    {
        text: "My Posts",
        url: "/myposts",
        back: true,
    },
    {   
        text: "My Favourite Posts",
        url: "/myfavourites",
        back: true,
    },
    {
        text: "Loans",
        url: "/categories/loans",
        back: true,
    },
    {
        text: "Sell & Buy",
        url: "/categories/sell",
        back: true,
    },
    {
        text: "Exchange",
        url: "/categories/exchange",
        back: true,
    },
    {
        text: "My profile",
        url: "/profile",
        back: true,
    },
    {
        text: "Add Post",
        url: "/newpost",
        back: true,
    },
    {
        text: "Services",
        url: "/categories/services",
        back: true,
    },
    {
        text: "Categories",
        url: "/categories",
        back: false,
    },
    {
        text: "View Post",
        url: "/post/view/:id",
        back: true,
    },
    {
        text: "Post",
        url: "/post/:id",
        back: true,
    },
    {
        text: "Edit Post",
        url: "/post/edit/:postId",
        back:true,
    }
]