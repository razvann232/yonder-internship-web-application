import styles from "./TopNavbar.module.scss";
import { ReactComponent as ArrowLeftLogo } from "../../assets/Icons/arrow-narrow-left.svg";
import { ReactComponent as BellLogo } from "../../assets/Icons/bell.svg";
import { ReactComponent as LogoutLogo } from "../../assets/Icons/logout.svg";
import { useNavigate } from "react-router-dom";
import { navBarItems } from "./MenuItems";
import { useLocation } from "react-router-dom";
import { useState, useEffect } from "react";
import { matchPattern } from "url-matcher";
import NotificationPopup from "../../components/notificationPopup/NotificationPopup.js";
import DeauthHandler from "../deauthHandler/DeauthHandler";
import RequestHelper from "../requestHelper/RequestHelper";
import { CONFIG } from "../../config/app.config";
import ErrorHandler from "../errorHandler/ErrorHandler";
import SessionStorage from "../sessionStorage/SessionStorage.ts";

const TopNavbar = () => {
  const HTTP = RequestHelper(CONFIG.SERVER_ADRESS);
  const navigator = useNavigate();
  const deauthHandler = DeauthHandler();
  const storage = SessionStorage();
  const token = CONFIG.PRODUCTION
    ? storage.getItem("token")
    : CONFIG.DEFAULT_DEBUGGING_USER_ID.toString();
  const handleClick = () => {
    deauthHandler.deauth();
  };
  const location = useLocation();
  const [title, setTitle] = useState("");
  const [arrowBack, setArrowBack] = useState(false);
  const [notifications, setNotifications] = useState([]);

  useEffect(() => {
    const foundTitle = navBarItems.find((element) =>
      matchPattern(element.url, location.pathname)
    );
    setTitle(foundTitle?.text);
    setArrowBack(foundTitle?.back);
    loadNotifications();
  });

  useEffect(() => {
    loadNotifications();
  }, [location]);

  const loadNotifications = () => {
    HTTP.GET("/notification", { auth: token }, (response, error) => {
      setNotifications(response.data);
    });
  };

  return (
    <div className={styles.container}>
      <div className={styles.containerLeft}>
        {arrowBack ? (
          <ArrowLeftLogo
            className={styles.arrowLeft}
            onClick={() => navigator(-1)}
          />
        ) : (
          ""
        )}
        <h1 className={styles.item}>{title}</h1>
      </div>
      <div className={styles.containerRight}>
        <NotificationPopup
          notifications={notifications}
          setNotifications={setNotifications}
        >
          <BellLogo
            className={`${styles.bellLogo} ${
              notifications.length ? styles.active : styles.inactive
            }`}
          />
        </NotificationPopup>
        <LogoutLogo className={styles.logoutLogo} onClick={handleClick} />
      </div>
    </div>
  );
};
export default TopNavbar;
