export const CONFIG = {
  PRODUCTION: true,
  DEFAULT_DEBUGGING_USER_ID: 1,
  SERVER_ADRESS:"http://localhost:8080",
};

export const categoryURLs = {
  SELL: "/categories/sell",
  LOANS: "/categories/loans",
  EXCHANGE: "/categories/exchange",
  SERVICES: "/categories/services",
};
