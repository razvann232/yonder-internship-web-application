package com.example.yoleicsbebogdan.Crons;

import com.example.yoleicsbebogdan.Entities.Notification;
import com.example.yoleicsbebogdan.Repositories.NotificationRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class NotificationCron {
    private final NotificationRepository notificationRepository;

    @Scheduled(cron = "${cron.expression.notification-delete}")
    public void deleteSeenNotifications(){
        System.out.println("Deleted notifications!");
        List<Notification> seenNotifications = notificationRepository.getNotificationBySeen(true);
        notificationRepository.deleteAll(seenNotifications);
    }
}
