package com.example.yoleicsbebogdan.Crons;

import com.example.yoleicsbebogdan.Entities.Post;
import com.example.yoleicsbebogdan.Repositories.PostRepository;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.List;

@Component
@AllArgsConstructor
public class PostCron {
    private final PostRepository postRepository;

    @Scheduled(cron = "${cron.expression.post-deactivate}")
    public void deactivateExpiredPosts(){
        System.out.println("Deactivated Posts!");
        List<Post> expiredPosts = postRepository.findAllByExpirationDateBefore(Instant.now());
        expiredPosts.forEach(post->{
            post.setActive(false);
        });
        postRepository.saveAll(expiredPosts);
    }
}
