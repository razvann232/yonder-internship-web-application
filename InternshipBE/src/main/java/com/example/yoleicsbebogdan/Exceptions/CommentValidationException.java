package com.example.yoleicsbebogdan.Exceptions;

public class CommentValidationException extends Exception{
    public CommentValidationException(String message) {
        super(message);
    }
}
