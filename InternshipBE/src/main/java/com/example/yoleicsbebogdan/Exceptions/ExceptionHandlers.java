package com.example.yoleicsbebogdan.Exceptions;

import com.example.yoleicsbebogdan.DTOs.DTO;
import com.example.yoleicsbebogdan.DTOs.Responses.ServerErrorDTO;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.io.IOException;

@ControllerAdvice
public class ExceptionHandlers {

    @ExceptionHandler({ConversionFailedException.class,BindException.class, MethodArgumentTypeMismatchException.class})
    public ResponseEntity<DTO> handleRequestAttributesConversionExceptions(Exception e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new ServerErrorDTO("Invalid data in one of the requested parameters!","INVALID_REQUEST")
        );
    }

    @ExceptionHandler(MissingRequestHeaderException.class)
    public ResponseEntity<DTO> handleMissingHeaderParameter(MissingRequestHeaderException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new ServerErrorDTO("Header parameter '"+e.getHeaderName()+"' is missing!","INVALID_REQUEST")
        );
    }

    @ExceptionHandler(UserValidationException.class)
    public ResponseEntity<DTO> handleUserValidationException (UserValidationException e){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ServerErrorDTO(e.getMessage(),"INVALID_INPUT"));
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<DTO> handleUserAlreadyExistsException(DataIntegrityViolationException e){
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(new ServerErrorDTO("User already exists!","DUPLICATE_ENTRY"));
    }

    @ExceptionHandler(UserAuthException.class)
    public ResponseEntity<DTO> handleAuthExceptions(UserAuthException e){
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(
                new ServerErrorDTO(e.getMessage(), "NO_ACCESS"));
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<DTO> handleUserNotFoundException(UserNotFoundException e){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ServerErrorDTO(e.getMessage(),"USER_NOT_FOUND"));
    }

    @ExceptionHandler(PostValidationException.class)
    public ResponseEntity<DTO> handlePostValidationException(PostValidationException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ServerErrorDTO(e.getMessage(),"INVALID_POST"));
    }
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<DTO> handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ServerErrorDTO(e.getMessage(),"INVALID_METHOD"));
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<DTO> handleIOException(IOException e) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ServerErrorDTO(e.getMessage(),"ERROR"));
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<DTO> handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ServerErrorDTO("Invalid data in body!","INVALID_DATA"));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<DTO> handleUncaughtException(Exception e) {
        String defaultMessage = "Something bad happened :(";
        e.printStackTrace();
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ServerErrorDTO(defaultMessage,"ERROR"));
    }
}