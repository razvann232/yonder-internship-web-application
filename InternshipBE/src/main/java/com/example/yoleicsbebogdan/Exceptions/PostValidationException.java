package com.example.yoleicsbebogdan.Exceptions;

public class PostValidationException  extends Exception{
    public PostValidationException(String message) {
        super(message);
    }
}
