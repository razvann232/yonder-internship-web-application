package com.example.yoleicsbebogdan.Exceptions;

public class UserAuthException extends Exception{
    public UserAuthException(String message) {
        super(message);
    }
}
