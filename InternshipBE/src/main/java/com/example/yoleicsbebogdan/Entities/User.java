package com.example.yoleicsbebogdan.Entities;

import com.example.yoleicsbebogdan.DTOs.IUser;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="USER")
@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements IUser {
    @Id
    @GeneratedValue
    @Column(name="ID")
    private Integer id;

    @NotNull
    @Column(name="NAME")
    private String name;

    @NotNull
    @Column(name="EMAIL",unique = true)
    @Pattern(regexp = "\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})")
    private String email;

    @NotNull
    @Column(name="PASSWORD")
    private String password;

    @NotNull
    @Column(name="PHONE_NUMBER",unique = true)
    @Pattern(regexp="0\\d{9}")
    private String phoneNumber;

    @NotNull
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "ROLE_ID")
    private Role role;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user",cascade = {CascadeType.ALL})
    private Set<Post> posts = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user",cascade = {CascadeType.ALL})
    private Set<Comment> comments = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user",cascade = {CascadeType.ALL})
    private Set<Notification> notifications = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user",cascade = {CascadeType.ALL})
    private Set<Favourite> favourites = new HashSet<>();

    @Column(name="gender")
    @Enumerated(EnumType.STRING)
    private UserGenderEnum gender;

    @OneToOne(mappedBy = "user",cascade = {CascadeType.ALL})
    private ProfilePicture profilePicture;
}
