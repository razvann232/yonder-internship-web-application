package com.example.yoleicsbebogdan.Entities;

import lombok.*;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="THUMBNAIL")
@Component
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "post")
@EqualsAndHashCode(exclude="post")
public class Thumbnail {
    @Id
    @Column(name="ID")
    @GeneratedValue
    private Integer id;

    @NotNull
    @OneToOne(mappedBy = "thumbnail",cascade = {CascadeType.DETACH})
    private Post post;

    @NotNull
    @Column(name = "CONTENT", unique = false, nullable = false, columnDefinition = "LONGBLOB")
    private byte[] content;

    @NotNull
    @Column(name="TYPE")
    private String type;
}
