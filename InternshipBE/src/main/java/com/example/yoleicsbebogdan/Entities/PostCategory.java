package com.example.yoleicsbebogdan.Entities;

import lombok.*;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="CATEGORY")
@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "post")
@EqualsAndHashCode(exclude = "post")
public class PostCategory {
    @Id
    @GeneratedValue
    @Column(name="ID")
    private Integer id;

    @NotNull
    @Column(name="TYPE")
    @Enumerated(EnumType.STRING)
    private PostCategoryEnum type;

    @NotNull
    @Column(name="DESCRIPTION")
    private String description;

    @NotNull
    @OneToOne(mappedBy = "category",cascade = {CascadeType.DETACH})
    private Post post;

}
