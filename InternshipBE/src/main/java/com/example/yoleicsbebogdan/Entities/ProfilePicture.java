package com.example.yoleicsbebogdan.Entities;

import lombok.*;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="PROFILE_PICTURE")
@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "user")
@EqualsAndHashCode(exclude="user")
public class ProfilePicture {
    @Id
    @Column(name="ID")
    @GeneratedValue
    private Integer id;

    @NotNull
    @OneToOne(cascade = {CascadeType.DETACH})
    @JoinColumn(name = "USER_ID")
    private User user;

    @NotNull
    @Column(name = "CONTENT", unique = false, nullable = false, columnDefinition = "LONGBLOB")
    private byte[] content;

    @NotNull
    @Column(name="TYPE")
    private String type;

}
