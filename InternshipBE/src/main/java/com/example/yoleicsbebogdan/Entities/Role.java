package com.example.yoleicsbebogdan.Entities;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="ROLE")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Role {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Integer id;

    @NotNull
    @Column(name="ROLE")
    @Enumerated(EnumType.STRING)
    private RoleEnum roleName;

    @NotNull
    @Column(name="DESCR")
    private String desc;

    @NotNull
    @OneToOne(mappedBy = "role",cascade = {CascadeType.DETACH})
    private User user;

    @NotNull
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "role",cascade = {CascadeType.ALL})
    private Set<Permissions> permissions = new HashSet<>();
}
