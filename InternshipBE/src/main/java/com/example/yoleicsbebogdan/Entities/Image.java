package com.example.yoleicsbebogdan.Entities;

import lombok.*;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="IMAGE")
@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "post")
@EqualsAndHashCode(exclude="post")
public class Image {
    @Id
    @Column(name="ID")
    @GeneratedValue
    private Integer id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH})
    @JoinColumn(name="POST_ID",referencedColumnName = "ID")
    private Post post;

    @NotNull
    @Column(name = "CONTENT", unique = false, nullable = false, columnDefinition = "LONGBLOB")
    private byte[] content;

    @NotNull
    @Column(name="TYPE")
    private String type;

}
