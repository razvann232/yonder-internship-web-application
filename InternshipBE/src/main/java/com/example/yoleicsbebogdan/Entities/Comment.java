package com.example.yoleicsbebogdan.Entities;

import com.example.yoleicsbebogdan.Configs.CommentValidationConstants;
import com.example.yoleicsbebogdan.Configs.PostValidationConstants;
import lombok.*;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Entity
@Table(name="COMMENT")
@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "post")
@EqualsAndHashCode(exclude={"post","user"})
public class Comment {
    @Id
    @GeneratedValue
    @Column(name="ID")
    private Integer id;

    @NotNull
    @Column(length = 200, name="CONTENT")
    private String content;

    @NotNull
    @Column(name="DATE")
    private Instant date;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY,cascade = {CascadeType.DETACH})
    @JoinColumn(name="POST_ID",referencedColumnName = "ID")
    private Post post;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY,cascade = {CascadeType.DETACH})
    @JoinColumn(name="USER_ID_FROM",referencedColumnName = "ID")
    private User user;
}
