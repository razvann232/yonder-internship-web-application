package com.example.yoleicsbebogdan.Entities;

import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;

@Entity
@ToString(exclude = "role")
@EqualsAndHashCode(exclude="role")
@Table(name="PERMISSIONS")
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class Permissions{
    @Id
    @GeneratedValue
    @Column(name="ID")
    private Integer id;

    @NotNull
    @Column(name="PERMISSION")
    private String permission;

    @NotNull
    @Column(name="DESCRIPTION")
    private String description;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY,cascade = {CascadeType.ALL})
    @JoinColumn(name="ROLE_ID",referencedColumnName = "ID")
    private Role role;

}
