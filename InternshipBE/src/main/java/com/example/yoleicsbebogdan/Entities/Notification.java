package com.example.yoleicsbebogdan.Entities;

import lombok.*;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name="NOTIFICATION")
@Component
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = "user")
@ToString(exclude = "user")
public class Notification {
    @Id
    @GeneratedValue
    @Column(name="ID")
    private Integer id;

    @Column(name="CONTENT")
    private String content;

    @Column(name="DATE")
    private Instant date;

    @Column(name="SEEN")
    private Boolean seen;

    @ManyToOne(fetch = FetchType.LAZY,cascade = {CascadeType.DETACH})
    @JoinColumn(name="USER_ID",referencedColumnName = "ID")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY,cascade = {CascadeType.DETACH})
    @JoinColumn(name="POST_ID",referencedColumnName = "ID")
    private Post post;
}
