package com.example.yoleicsbebogdan.Entities;

import com.example.yoleicsbebogdan.Configs.PostValidationConstants;
import lombok.*;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="POST")
@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"user","favourites","notifications"})
@ToString(exclude = {"user","favourites","notifications"})
public class Post {
    @Id
    @Column(name="ID")
    @GeneratedValue
    private Integer id;

    @NotNull
    @Column(length = 1000, name="DESCRIPTION")
    private String description;

    @NotNull
    @Column(name="TITLE")
    private String title;

    @NotNull
    @Column(name="PRICE")
    private float price;

    @Column(name="ACTIVE")
    private boolean active;

    @NotNull
    @Column(name="CREATED_AT")
    private Instant createdAt;

    @NotNull
    @Column(name="END_DATE")
    private Instant expiresAt;

    @Column(name="VIEWS_NO")
    private int viewsNo;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "post",cascade = {CascadeType.ALL})
    private Set<Image> images = new HashSet<>();

    @NotNull
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "THUMBNAIL_ID")
    private Thumbnail thumbnail;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "post",cascade = {CascadeType.ALL})
    private Set<Comment> comments = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY,cascade = {CascadeType.DETACH})
    @JoinColumn(name="USER_ID",referencedColumnName = "ID")
    private User user;

    @NotNull
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "TYPE_ID")
    private PostCategory category;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "post",cascade = {CascadeType.ALL})
    private Set<Favourite> favourites = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "post",cascade = {CascadeType.ALL})
    private Set<Notification> notifications = new HashSet<>();

}
