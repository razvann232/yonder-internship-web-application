package com.example.yoleicsbebogdan.Repositories;

import com.example.yoleicsbebogdan.Entities.Post;
import com.example.yoleicsbebogdan.Entities.PostCategory;
import com.example.yoleicsbebogdan.Entities.PostCategoryEnum;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

@Repository
public interface PostRepository extends CrudRepository<Post, Integer> {
    List<Post> findPostsByCategoryTypeAndActive(PostCategoryEnum type, Boolean active);

    Integer countAllByCategoryTypeAndActive(PostCategoryEnum type, Boolean active);

    @Query("select p from Post p where p.expiresAt <= :expiringDate")
    List<Post> findAllByExpirationDateBefore(@Param("expiringDate") Instant expiringDate);
}
