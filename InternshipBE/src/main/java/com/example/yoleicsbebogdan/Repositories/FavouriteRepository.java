package com.example.yoleicsbebogdan.Repositories;

import com.example.yoleicsbebogdan.Entities.Favourite;
import com.example.yoleicsbebogdan.Entities.Post;
import com.example.yoleicsbebogdan.Entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FavouriteRepository extends CrudRepository<Favourite, Integer> {
    Optional<Favourite> findByPostAndUser(Post post, User user);
}
