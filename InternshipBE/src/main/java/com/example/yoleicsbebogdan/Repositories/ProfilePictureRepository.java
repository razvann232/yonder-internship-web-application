package com.example.yoleicsbebogdan.Repositories;

import com.example.yoleicsbebogdan.Entities.ProfilePicture;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfilePictureRepository extends CrudRepository<ProfilePicture, Integer> {
}
