package com.example.yoleicsbebogdan.Repositories;

import com.example.yoleicsbebogdan.Entities.PostCategory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostCategoryRepository extends CrudRepository<PostCategory, Integer> {
}
