package com.example.yoleicsbebogdan.Repositories;

import com.example.yoleicsbebogdan.Entities.Thumbnail;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ThumbnailRepository extends CrudRepository<Thumbnail, Integer> {
}
