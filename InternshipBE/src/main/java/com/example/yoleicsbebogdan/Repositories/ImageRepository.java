package com.example.yoleicsbebogdan.Repositories;

import com.example.yoleicsbebogdan.Entities.Image;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageRepository extends CrudRepository<Image, Integer> {
}
