package com.example.yoleicsbebogdan.Repositories;

import com.example.yoleicsbebogdan.Entities.Notification;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationRepository extends CrudRepository<Notification, Integer> {
    List<Notification> getNotificationBySeen(Boolean seen);
}
