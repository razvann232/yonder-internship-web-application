package com.example.yoleicsbebogdan.Repositories;

import com.example.yoleicsbebogdan.Entities.Comment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Integer> {
}
