package com.example.yoleicsbebogdan.Services;

import com.example.yoleicsbebogdan.DTOs.DTO;
import com.example.yoleicsbebogdan.DTOs.Requests.NewCommentDTO;
import com.example.yoleicsbebogdan.DTOs.Responses.CommentSuccessDTO;
import com.example.yoleicsbebogdan.Entities.Comment;
import com.example.yoleicsbebogdan.Entities.Post;
import com.example.yoleicsbebogdan.Entities.User;
import com.example.yoleicsbebogdan.Exceptions.CommentValidationException;
import com.example.yoleicsbebogdan.Exceptions.PostValidationException;
import com.example.yoleicsbebogdan.Exceptions.UserAuthException;
import com.example.yoleicsbebogdan.Repositories.CommentRepository;
import com.example.yoleicsbebogdan.Repositories.PostRepository;
import com.example.yoleicsbebogdan.Utils.Validators.CommentValidator;
import com.example.yoleicsbebogdan.Utils.Validators.UserValidator;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CommentService {
    private UserValidator userValidator;
    private PostRepository postRepository;
    private CommentRepository commentRepository;
    private CommentValidator commentValidator;
    private ModelMapper mapper;
    private NotificationService notificationService;

    public ResponseEntity<DTO> addNewComment(Integer postId, Integer userId, NewCommentDTO body) throws UserAuthException, CommentValidationException, PostValidationException {
        User user = userValidator.authUser(userId);
        commentValidator.validateComment(body);
        try {
            Optional<Post> post = postRepository.findById(postId);
            if(post.isEmpty()){
                throw new PostValidationException("Post with given ID not found!");
            }

            Comment newComment = new Comment();
            newComment.setContent(body.getContent());
            newComment.setPost(post.get());
            newComment.setDate(Instant.now());
            newComment.setUser(user);
            Comment addedComment = commentRepository.save(newComment);

            notificationService.saveNotification(user.getName()+" left a comment on "+post.get().getTitle(),post.get(),user);

            return ResponseEntity.status(HttpStatus.CREATED).body(mapper.map(addedComment, CommentSuccessDTO.class));

        }catch (IllegalArgumentException e){
            throw new PostValidationException("Post ID cannot be null!");
        }
    }
}
