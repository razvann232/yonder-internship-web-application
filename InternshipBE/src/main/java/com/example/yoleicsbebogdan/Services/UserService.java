package com.example.yoleicsbebogdan.Services;

import com.example.yoleicsbebogdan.DTOs.DTO;
import com.example.yoleicsbebogdan.DTOs.Requests.UserLoginDTO;
import com.example.yoleicsbebogdan.DTOs.Requests.UserRegisterDTO;
import com.example.yoleicsbebogdan.DTOs.Requests.UserUpdateDTO;
import com.example.yoleicsbebogdan.DTOs.Responses.ServerErrorDTO;
import com.example.yoleicsbebogdan.DTOs.Responses.UserDTO;
import com.example.yoleicsbebogdan.Entities.ProfilePicture;
import com.example.yoleicsbebogdan.Entities.User;
import com.example.yoleicsbebogdan.Exceptions.PostValidationException;
import com.example.yoleicsbebogdan.Exceptions.UserAuthException;
import com.example.yoleicsbebogdan.Exceptions.UserNotFoundException;
import com.example.yoleicsbebogdan.Exceptions.UserValidationException;
import com.example.yoleicsbebogdan.Repositories.ProfilePictureRepository;
import com.example.yoleicsbebogdan.Repositories.UserRepository;
import com.example.yoleicsbebogdan.Utils.Factories.ImageFactory;
import com.example.yoleicsbebogdan.Utils.Factories.UserFactory;
import com.example.yoleicsbebogdan.Utils.Validators.PostValidator;
import com.example.yoleicsbebogdan.Utils.Validators.UserValidator;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final ProfilePictureRepository profilePictureRepository;
    private final ModelMapper mapper;
    private final PasswordEncoder bcrypt;
    private final UserValidator userValidator;
    private final UserFactory userFactory;
    private final ImageFactory imageFactory;
    private final PostValidator postValidator;


    public ResponseEntity<DTO> updateUser(UserUpdateDTO body, Integer userId) throws UserAuthException, UserValidationException, DataIntegrityViolationException {
        User user = userValidator.authUser(userId);
        User NewUser = userFactory.updateExistingUser(user,body);
        userValidator.validateUser(NewUser,true);
        User response = userRepository.save(NewUser);
        return ResponseEntity.ok().body(mapper.map(response,UserDTO.class));
    }


    public ResponseEntity<DTO> saveProfilePicture(MultipartFile image, Integer userId) throws UserAuthException, IOException, PostValidationException {
        User user = userValidator.authUser(userId);
        postValidator.validateImages(new MultipartFile[]{image});
        ProfilePicture newProfilePicture = imageFactory.createNewProfilePicture(image);
        ProfilePicture oldProfilePicture = user.getProfilePicture();

        if (oldProfilePicture == null) {
            newProfilePicture.setUser(user);
            user.setProfilePicture(newProfilePicture);
            User response = userRepository.save(user);
            return ResponseEntity.ok().body(mapper.map(response, UserDTO.class));
        }

        oldProfilePicture.setContent(newProfilePicture.getContent());
        oldProfilePicture.setType(newProfilePicture.getType());

        profilePictureRepository.save(oldProfilePicture);
        Optional<User> response = userRepository.findById(user.getId());
        return ResponseEntity.ok().body(mapper.map(response.get(), UserDTO.class));

    }

    public ResponseEntity<DTO> registerUser(UserRegisterDTO userRegisterDTO) throws UserValidationException, DataIntegrityViolationException {
        //validate the password before hashing it
        userValidator.validatePassword(userRegisterDTO.getPassword());
        User newUser = userFactory.createNewUser(userRegisterDTO);

        //ignore the password validation because it is hashed here
        userValidator.validateUser(newUser, true);
        User savedUser = userRepository.save(newUser);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(mapper.map(savedUser, UserDTO.class));
    }

    public ResponseEntity<DTO> loginUser(UserLoginDTO userLoginDTO) throws UserNotFoundException, UserValidationException {
        userValidator.validateUser(userLoginDTO, true);
        Optional<User> requestedUser = userRepository.findByEmail(userLoginDTO.getEmail());

        if (requestedUser.isEmpty()) {
            throw new UserNotFoundException("Wrong email or password!");
        }

        User attemptedUser = requestedUser.get();

        if (bcrypt.matches(userLoginDTO.getPassword(), attemptedUser.getPassword())) {
            UserDTO userDTO = mapper.map(attemptedUser, UserDTO.class);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(userDTO);
        } else {
            throw new UserNotFoundException("Wrong email or password!");
        }

    }

    public ResponseEntity<DTO> deleteProfilePicture(Integer userId) throws UserAuthException {
        User user = userValidator.authUser(userId);
        if(user.getProfilePicture() == null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ServerErrorDTO("The user does not have any profile picture","NOT_FOUND")
            );
        }
        ProfilePicture profilePicture = user.getProfilePicture();
        user.setProfilePicture(null);
        userRepository.save(user);
        profilePictureRepository.delete(profilePicture);
        return ResponseEntity.ok().body(null);
    }
}
