package com.example.yoleicsbebogdan.Services;

import com.example.yoleicsbebogdan.DTOs.DTO;
import com.example.yoleicsbebogdan.DTOs.Requests.NewPostDTO;
import com.example.yoleicsbebogdan.DTOs.Responses.*;
import com.example.yoleicsbebogdan.Entities.*;
import com.example.yoleicsbebogdan.Exceptions.PostValidationException;
import com.example.yoleicsbebogdan.Exceptions.UserAuthException;
import com.example.yoleicsbebogdan.Repositories.*;
import com.example.yoleicsbebogdan.Utils.Factories.ImageFactory;
import com.example.yoleicsbebogdan.Utils.Factories.PostFactory;
import com.example.yoleicsbebogdan.Utils.TimeBetweenHelper;
import com.example.yoleicsbebogdan.Utils.Validators.PostValidator;
import com.example.yoleicsbebogdan.Utils.Validators.UserValidator;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.*;

@Service
@AllArgsConstructor
public class PostService {
    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final PostFactory postFactory;
    private final ImageFactory imageFactory;
    private final ImageRepository imageRepository;
    private final ModelMapper mapper;
    private final PostValidator postValidator;
    private final UserValidator userValidator;
    private final FavouriteRepository favouriteRepository;
    private final ThumbnailRepository thumbnailRepository;
    private final NotificationService notificationService;
    private final PostCategoryRepository postCategoryRepository;
    private final TimeBetweenHelper timeBetweenHelper;

    public ResponseEntity<List<DTO>> getPostByCategory(Integer userId, PostCategoryEnum type) throws UserAuthException {
        User user = userValidator.authUser(userId);
        List<Post> posts = postRepository.findPostsByCategoryTypeAndActive(type, true);
        List<DTO> returnedPosts = new ArrayList<>();
        posts.forEach(post -> {
            PostMediumWithLikedDTO returnedPost = mapper.map(postFactory.mapPostToMediumDTO(post), PostMediumWithLikedDTO.class);
            returnedPost.setLiked(false);
            Optional<Favourite> dbFavouriteQuery = favouriteRepository.findByPostAndUser(post, user);
            if (dbFavouriteQuery.isPresent()) {
                //means that we have an entry with the given value in the db (the user likes the post)
                returnedPost.setLiked(true);
            }
            returnedPosts.add(returnedPost);
        });

        return ResponseEntity.ok().body(returnedPosts);
    }

    public ResponseEntity<DTO> setActivePost(Integer userId, Integer postId, Boolean active) throws UserAuthException, PostValidationException {
        User user = userValidator.authUser(userId);
        Optional<Post> response = postRepository.findById(postId);
        if (response.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ServerErrorDTO("The post with the given id does not exist!", "NOT_FOUND"));
        }
        Post post = response.get();
        if(!Objects.equals(post.getUser().getId(), user.getId())){
            throw new UserAuthException("You are not allowed to edit this post!");
        }
        if(post.getExpiresAt().isBefore(Instant.now())){
            throw new PostValidationException("Date must be in the future!");
        }
        post.setActive(active);
        postRepository.save(post);
        return ResponseEntity.ok().body(null);
    }

    public ResponseEntity<DTO> getPostById(Integer userId, Integer postId) throws UserAuthException {
        User user = userValidator.authUser(userId);
        Optional<Post> response = postRepository.findById(postId);
        if (response.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ServerErrorDTO("The post with the given id does not exist!", "NOT_FOUND"));
        }


        Post post = response.get();

        Optional<Favourite> isLiked = favouriteRepository.findByPostAndUser(post,user);

        post.setViewsNo(post.getViewsNo() + 1);
        Post latestPost = postRepository.save(post);
        PostResponseDTO postDTO = mapper.map(latestPost, PostResponseDTO.class);
        Long[] timeBetween = timeBetweenHelper.getDaysHoursMinutesBetween(latestPost.getCreatedAt(),latestPost.getExpiresAt());
        postDTO.setTimeLeftDays(timeBetween[0]);
        postDTO.setTimeLeftHours(timeBetween[1]);
        postDTO.setTimeLeftMinutes(timeBetween[2]);
        postDTO.setLiked(isLiked.isPresent());
        return ResponseEntity.status(HttpStatus.OK).body(postDTO);
    }

    public ResponseEntity<DTO> addNewPost(NewPostDTO formData, Integer userId) throws PostValidationException, UserAuthException {
        postValidator.validatePost(formData);
        User author = userValidator.authUser(userId);

        Post postToBeInserted = postFactory.createNewPost(formData, author);
        Post responsePost = postRepository.save(postToBeInserted);
        return ResponseEntity.status(HttpStatus.CREATED).body(mapper.map(responsePost, PostResponseDTO.class));

    }

    public ResponseEntity<List<DTO>> getPostByUserID(Integer userId) throws UserAuthException {
        User user = userValidator.authUser(userId);
        Set<Post> posts = user.getPosts();
        List<DTO> postsDTO = postFactory.mapPostsToMediumWithActiveDTO(new HashSet<>(posts));

        return ResponseEntity.ok().body(postsDTO);
    }

    public ResponseEntity<List<DTO>> getFavouritePosts(Integer userId) throws UserAuthException {
        User user = userValidator.authUser(userId);
        List<DTO> posts = new ArrayList<>();
        user.getFavourites().forEach(favourite -> {
            Post post = favourite.getPost();
            PostMediumWithActiveDTO postDTO = mapper.map(postFactory.mapPostToMediumDTO(post), PostMediumWithActiveDTO.class);
            postDTO.setActive(post.isActive());
            posts.add(postDTO);
        });
        return ResponseEntity.ok().body(posts);
    }

    public ResponseEntity<DTO> likePost(Integer userId, Integer postId) throws UserAuthException, PostValidationException {
        User user = userValidator.authUser(userId);
        Post post = postValidator.checkIfPostExists(postId);
        Optional<Favourite> dbFavouriteQuery = favouriteRepository.findByPostAndUser(post,user);
        if(dbFavouriteQuery.isEmpty()){
            Favourite favouriteEntry = new Favourite();
            favouriteEntry.setPost(post);
            favouriteEntry.setUser(user);
            favouriteRepository.save(favouriteEntry);
        }
        return ResponseEntity.ok().body(null);
    }

    public ResponseEntity<DTO> unLikePost(Integer userId, Integer postId) throws UserAuthException, PostValidationException {
        User user = userValidator.authUser(userId);
        Post post = postValidator.checkIfPostExists(postId);
        Optional<Favourite> dbFavouriteQuery = favouriteRepository.findByPostAndUser(post,user);
        dbFavouriteQuery.ifPresent(favouriteRepository::delete);
        return ResponseEntity.ok().body(null);
    }

    public ResponseEntity<DTO> updatePost(NewPostDTO formData, Integer userId, Integer postId) throws PostValidationException, UserAuthException {
        postValidator.validatePost(formData);
        User user = userValidator.authUser(userId);
        Post post = postValidator.checkIfPostExists(postId);

        if(!Objects.equals(post.getUser().getId(), user.getId())){
            throw new UserAuthException("You are not allowed to edit this post!");
        }

        String oldTitle = post.getTitle();
        Set<Image> oldImages = post.getImages();
        Thumbnail oldThumbnail = post.getThumbnail();
        PostCategory oldCategory = post.getCategory();

        Post postToBeInserted = postFactory.updateOldPost(post,formData);
        Post responsePost = postRepository.save(postToBeInserted);

        imageRepository.deleteAll(oldImages);
        thumbnailRepository.delete(oldThumbnail);
        postCategoryRepository.delete(oldCategory);

        notificationService.saveNotification(user.getName()+" updated "+oldTitle+" post.",responsePost,user);

        return ResponseEntity.ok().body(mapper.map(responsePost, PostResponseDTO.class));
    }


    @Transactional
    public ResponseEntity<DTO> getPostCount(int userId) throws UserAuthException{
        userValidator.authUser(userId);

        PostCountDTO postCountDTO = new PostCountDTO();

        Integer numberSellPosts = postRepository.countAllByCategoryTypeAndActive(PostCategoryEnum.SELL, true);
        Integer numberExchangePosts = postRepository.countAllByCategoryTypeAndActive(PostCategoryEnum.EXCHANGE, true);
        Integer numberLoansPosts = postRepository.countAllByCategoryTypeAndActive(PostCategoryEnum.LOANS, true);
        Integer numberServicesPosts = postRepository.countAllByCategoryTypeAndActive(PostCategoryEnum.SERVICES, true);

        postCountDTO.setNumberSellPosts(numberSellPosts);
        postCountDTO.setNumberExchangePosts(numberExchangePosts);
        postCountDTO.setNumberLoansPosts(numberLoansPosts);
        postCountDTO.setNumberServicesPosts(numberServicesPosts);

        return new ResponseEntity<>(postCountDTO, HttpStatus.OK);
    }

}
