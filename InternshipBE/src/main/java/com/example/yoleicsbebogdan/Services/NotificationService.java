package com.example.yoleicsbebogdan.Services;

import com.example.yoleicsbebogdan.Crons.NotificationCron;
import com.example.yoleicsbebogdan.DTOs.DTO;
import com.example.yoleicsbebogdan.DTOs.Responses.ImageDTO;
import com.example.yoleicsbebogdan.DTOs.Responses.NotificationDTO;
import com.example.yoleicsbebogdan.DTOs.Responses.ServerErrorDTO;
import com.example.yoleicsbebogdan.Entities.Notification;
import com.example.yoleicsbebogdan.Entities.Post;
import com.example.yoleicsbebogdan.Entities.User;
import com.example.yoleicsbebogdan.Exceptions.UserAuthException;
import com.example.yoleicsbebogdan.Repositories.NotificationRepository;
import com.example.yoleicsbebogdan.Utils.Validators.UserValidator;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class NotificationService {
    private UserValidator userValidator;
    private ModelMapper mapper;
    private NotificationRepository notificationRepository;

    public ResponseEntity<List<NotificationDTO>> getNotifications(Integer userId) throws UserAuthException {
        User user = userValidator.authUser(userId);
        List<Notification> unSeenNotifications = user
                .getNotifications()
                .stream()
                .filter(notification -> !notification.getSeen())
                .toList();

        List<NotificationDTO> notifications = unSeenNotifications
                .stream()
                .map(notification -> {
                    NotificationDTO returnedNotification = mapper.map(notification, NotificationDTO.class);
                    ImageDTO thumbnail = mapper.map(notification.getPost().getThumbnail(), ImageDTO.class);
                    returnedNotification.setThumbnail(thumbnail);
                    return returnedNotification;
                })
                .toList();

        return ResponseEntity.ok().body(notifications);
    }

    public Notification saveNotification(String message, Post post, User user) {
        Notification notification = new Notification();
        notification.setContent(message);
        notification.setDate(Instant.now());
        notification.setPost(post);
        notification.setUser(user);
        notification.setSeen(false);
        return notificationRepository.save(notification);
    }

    public ResponseEntity<DTO> seeNotification(Integer userId, Integer notificationId) throws UserAuthException {
        userValidator.authUser(userId);
        Optional<Notification> response = notificationRepository.findById(notificationId);
        if (response.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ServerErrorDTO("The notification with the given id does not exist", "NOT_FOUND")
            );
        }
        Notification notification = response.get();
        notification.setSeen(true);
        notificationRepository.save(notification);
        return ResponseEntity.ok().body(null);
    }
}
