package com.example.yoleicsbebogdan.Configs;

public class PostValidationConstants {
    public final static Integer TITLE_MIN_LEN = 3;
    public final static Integer TITLE_MAX_LEN = 20;
    public final static Integer DESCRIPTION_MIN_LEN = 10;
    public final static Integer DESCRIPTION_MAX_LEN = 1000;
}
