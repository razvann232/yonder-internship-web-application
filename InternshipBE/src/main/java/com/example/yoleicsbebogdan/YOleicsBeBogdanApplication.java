package com.example.yoleicsbebogdan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YOleicsBeBogdanApplication {

    public static void main(String[] args) {
        SpringApplication.run(YOleicsBeBogdanApplication.class, args);
    }

}
