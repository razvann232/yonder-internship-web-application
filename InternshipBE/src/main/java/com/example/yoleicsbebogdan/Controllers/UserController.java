package com.example.yoleicsbebogdan.Controllers;

import com.example.yoleicsbebogdan.DTOs.DTO;
import com.example.yoleicsbebogdan.DTOs.Requests.UserLoginDTO;
import com.example.yoleicsbebogdan.DTOs.Requests.UserRegisterDTO;
import com.example.yoleicsbebogdan.DTOs.Requests.UserUpdateDTO;
import com.example.yoleicsbebogdan.Exceptions.PostValidationException;
import com.example.yoleicsbebogdan.Exceptions.UserAuthException;
import com.example.yoleicsbebogdan.Exceptions.UserNotFoundException;
import com.example.yoleicsbebogdan.Exceptions.UserValidationException;
import com.example.yoleicsbebogdan.Services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@AllArgsConstructor
@CrossOrigin
public class UserController {

    private final UserService service;

    @PostMapping("/user/save")
    public ResponseEntity<DTO> postUser(@RequestBody UserRegisterDTO user) throws UserValidationException, DataIntegrityViolationException {
        return service.registerUser(user);
    }

    @PostMapping("/user/login")
    public ResponseEntity<DTO> loginUser(@RequestBody UserLoginDTO user) throws UserNotFoundException, UserValidationException {
        return service.loginUser(user);
    }

    @PostMapping("/user/profile-picture/save")
    public ResponseEntity<DTO> saveProfilePicture(@RequestParam("image") MultipartFile file, @RequestHeader(name = "auth") Integer userId) throws IOException, UserAuthException, PostValidationException {
        return service.saveProfilePicture(file, userId);
    }

    @DeleteMapping("/user/profile-picture/delete")
    public ResponseEntity<DTO> saveProfilePicture(@RequestHeader(name = "auth") Integer userId) throws UserAuthException {
        return service.deleteProfilePicture(userId);
    }

    @PutMapping("/user/update")
    public ResponseEntity<DTO> updateUser(@RequestBody UserUpdateDTO body, @RequestHeader(name = "auth") Integer userId) throws UserAuthException, UserValidationException, DataIntegrityViolationException {
        return service.updateUser(body, userId);
    }
}