package com.example.yoleicsbebogdan.Controllers;

import com.example.yoleicsbebogdan.DTOs.DTO;
import com.example.yoleicsbebogdan.DTOs.Requests.NewCommentDTO;
import com.example.yoleicsbebogdan.Exceptions.CommentValidationException;
import com.example.yoleicsbebogdan.Exceptions.PostValidationException;
import com.example.yoleicsbebogdan.Exceptions.UserAuthException;
import com.example.yoleicsbebogdan.Services.CommentService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@CrossOrigin
public class CommentController {
    private CommentService commentService;
    @PostMapping("/comment/save/{id}")
    public ResponseEntity<DTO> addNewComment(@PathVariable(name="id") Integer postId, @RequestBody NewCommentDTO body, @RequestHeader(name = "auth") Integer userId) throws CommentValidationException, UserAuthException, PostValidationException {
        return commentService.addNewComment(postId,userId,body);
    }
}
