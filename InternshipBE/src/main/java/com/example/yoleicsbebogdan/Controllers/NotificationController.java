package com.example.yoleicsbebogdan.Controllers;

import com.example.yoleicsbebogdan.DTOs.DTO;
import com.example.yoleicsbebogdan.DTOs.Responses.NotificationDTO;
import com.example.yoleicsbebogdan.Exceptions.UserAuthException;
import com.example.yoleicsbebogdan.Services.NotificationService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@CrossOrigin
public class NotificationController {
    private NotificationService notificationService;
    @GetMapping("/notification")
    public ResponseEntity<List<NotificationDTO>> getNotifications(@RequestHeader(name = "auth") Integer userId) throws UserAuthException {
        return notificationService.getNotifications(userId);
    }
    @PutMapping("/notification/seen/{notification_id}")
    public ResponseEntity<DTO> seeNotification(@RequestHeader(name = "auth") Integer userId, @PathVariable(name="notification_id") Integer notificationId) throws UserAuthException {
        return notificationService.seeNotification(userId,notificationId);
    }
}
