package com.example.yoleicsbebogdan.Controllers;

import com.example.yoleicsbebogdan.DTOs.DTO;
import com.example.yoleicsbebogdan.DTOs.Requests.NewPostDTO;
import com.example.yoleicsbebogdan.Entities.PostCategoryEnum;
import com.example.yoleicsbebogdan.Exceptions.PostValidationException;
import com.example.yoleicsbebogdan.Exceptions.UserAuthException;
import com.example.yoleicsbebogdan.Services.PostService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@CrossOrigin
public class PostController {
    private final PostService service;

    @PostMapping("/post/save")
    public ResponseEntity<DTO> addNewPost(@ModelAttribute NewPostDTO req, @RequestHeader(name = "auth") Integer userId) throws PostValidationException, UserAuthException {
        return service.addNewPost(req, userId);
    }

    @PutMapping("/post/update/{id}")
    public ResponseEntity<DTO> updatePost(@ModelAttribute NewPostDTO req, @PathVariable(name = "id") Integer postId, @RequestHeader(name = "auth") Integer userId) throws PostValidationException, UserAuthException {
        return service.updatePost(req, userId, postId);
    }

    @PutMapping("/post/active-set/{id}/{value}")
    public ResponseEntity<DTO> activatePost(@PathVariable(name = "id") Integer postId,@PathVariable(name = "value") Boolean value, @RequestHeader(name = "auth") Integer userId) throws PostValidationException, UserAuthException {
        return service.setActivePost(userId,postId,value);
    }

    @GetMapping("/post/{id}")
    public ResponseEntity<DTO> getPostById(@RequestHeader(name = "auth") Integer userId, @PathVariable(name = "id") Integer postId) throws UserAuthException {
        return service.getPostById(userId, postId);
    }

    @GetMapping("/post/all/{category}")
    public ResponseEntity<List<DTO>> getPostByCategory(@RequestHeader(name = "auth") Integer userId, @PathVariable(name = "category") PostCategoryEnum type) throws UserAuthException {
        return service.getPostByCategory(userId, type);
    }

    @GetMapping("/post/my-posts")
    private ResponseEntity<List<DTO>> getPostsByUserId(@RequestHeader("auth") Integer userID) throws UserAuthException {
        return service.getPostByUserID(userID);
    }

    @GetMapping("/post/my-favourites")
    private ResponseEntity<List<DTO>> getFavouritePosts(@RequestHeader("auth") Integer userID) throws UserAuthException {
        return service.getFavouritePosts(userID);
    }

    @PostMapping("/post/like/{id}")
    public ResponseEntity<DTO> likePost(@RequestHeader(name = "auth") Integer userId, @PathVariable(name = "id") Integer postId) throws UserAuthException, PostValidationException {
        return service.likePost(userId, postId);
    }

    @DeleteMapping("/post/unlike/{id}")
    public ResponseEntity<DTO> unLikePost(@RequestHeader(name = "auth") Integer userId, @PathVariable(name = "id") Integer postId) throws UserAuthException, PostValidationException {
        return service.unLikePost(userId, postId);
    }

    @GetMapping("/post/count")
    private ResponseEntity<DTO> getPostCount(@RequestHeader("auth") String userID) throws UserAuthException {
        return service.getPostCount(Integer.parseInt(userID));
    }
}
