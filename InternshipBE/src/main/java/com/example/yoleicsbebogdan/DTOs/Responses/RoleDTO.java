package com.example.yoleicsbebogdan.DTOs.Responses;

import com.example.yoleicsbebogdan.DTOs.DTO;
import com.example.yoleicsbebogdan.Entities.RoleEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleDTO implements DTO {

    private Integer id;
    private RoleEnum roleName;
    private String desc;
    private Set<PermissionsDTO> permissions;
}
