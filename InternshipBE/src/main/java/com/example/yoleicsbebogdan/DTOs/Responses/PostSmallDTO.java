package com.example.yoleicsbebogdan.DTOs.Responses;

import lombok.Data;

@Data
public class PostSmallDTO {
    private Integer id;
    private String title;
    private String description;
    private Float price;
}
