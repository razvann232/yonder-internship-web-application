package com.example.yoleicsbebogdan.DTOs.Responses;

import com.example.yoleicsbebogdan.DTOs.DTO;
import com.example.yoleicsbebogdan.DTOs.IUser;
import com.example.yoleicsbebogdan.Entities.UserGenderEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO implements DTO, IUser {

    private Integer id;
    private String name;
    private String email;
    private String phoneNumber;
    private RoleDTO role;
    private UserGenderEnum gender;
    private ImageDTO profilePicture;
}
