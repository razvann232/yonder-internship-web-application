package com.example.yoleicsbebogdan.DTOs.Responses;

import com.example.yoleicsbebogdan.DTOs.DTO;
import lombok.Data;

@Data
public class PostCountDTO implements DTO {
    private Integer numberSellPosts;
    private Integer numberExchangePosts;
    private Integer numberLoansPosts;
    private Integer numberServicesPosts;
}
