package com.example.yoleicsbebogdan.DTOs.Responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostMediumWithActiveDTO extends PostMediumDTO{
    private Boolean active;
}
