package com.example.yoleicsbebogdan.DTOs.Responses;
import com.example.yoleicsbebogdan.DTOs.DTO;
import com.example.yoleicsbebogdan.DTOs.IUser;
import com.example.yoleicsbebogdan.Entities.ProfilePicture;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserMinifiedDTO implements DTO, IUser {

    private Integer id;
    private String name;
    private String email;
    private String phoneNumber;
    private ImageDTO profilePicture;

}
