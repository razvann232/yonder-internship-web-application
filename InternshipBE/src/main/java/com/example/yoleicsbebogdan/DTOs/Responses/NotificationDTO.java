package com.example.yoleicsbebogdan.DTOs.Responses;

import com.example.yoleicsbebogdan.DTOs.DTO;
import com.example.yoleicsbebogdan.Entities.Thumbnail;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NotificationDTO implements DTO {
    private Integer Id;
    private String content;
    private Instant date;
    private Boolean seen;
    private ImageDTO thumbnail;
}
