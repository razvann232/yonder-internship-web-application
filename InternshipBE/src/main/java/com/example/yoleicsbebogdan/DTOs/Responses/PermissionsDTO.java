package com.example.yoleicsbebogdan.DTOs.Responses;

import com.example.yoleicsbebogdan.DTOs.DTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PermissionsDTO implements DTO {

    private Integer id;
    private String permission;
    private String description;
}
