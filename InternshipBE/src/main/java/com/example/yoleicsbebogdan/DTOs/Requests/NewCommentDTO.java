package com.example.yoleicsbebogdan.DTOs.Requests;

import lombok.Data;

@Data
public class NewCommentDTO {
    private String content;
}
