package com.example.yoleicsbebogdan.DTOs.Requests;

import com.example.yoleicsbebogdan.DTOs.DTO;
import com.example.yoleicsbebogdan.Entities.PostCategoryEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewPostDTO implements DTO {
    private String title;
    private Float price;
    private String description;
    @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private Instant expiresAt;
    private PostCategoryEnum category;
    private MultipartFile[] images;
}
