package com.example.yoleicsbebogdan.DTOs.Requests;

import com.example.yoleicsbebogdan.DTOs.DTO;
import com.example.yoleicsbebogdan.DTOs.IUser;
import com.example.yoleicsbebogdan.Entities.UserGenderEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserUpdateDTO implements DTO, IUser {

    private String name;
    private String email;
    private String phoneNumber;
    private String password;
    private UserGenderEnum gender;

}