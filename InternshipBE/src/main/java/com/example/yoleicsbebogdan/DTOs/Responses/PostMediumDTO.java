package com.example.yoleicsbebogdan.DTOs.Responses;

import com.example.yoleicsbebogdan.DTOs.DTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostMediumDTO implements DTO {
    private Integer id;
    private String title;
    private Long timeLeftDays;
    private Long timeLeftHours;
    private Long timeLeftMinutes;
    private Float price;
    private ImageDTO thumbnail;
    private Instant createdAt;
    private Instant expiresAt;
}
