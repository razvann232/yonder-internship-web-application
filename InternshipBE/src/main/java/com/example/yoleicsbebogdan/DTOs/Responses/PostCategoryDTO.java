package com.example.yoleicsbebogdan.DTOs.Responses;

import com.example.yoleicsbebogdan.Entities.PostCategoryEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostCategoryDTO {
    private Integer id;
    private PostCategoryEnum type;
    private String description;
}
