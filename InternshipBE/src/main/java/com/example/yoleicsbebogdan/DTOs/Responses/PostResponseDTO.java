package com.example.yoleicsbebogdan.DTOs.Responses;

import com.example.yoleicsbebogdan.DTOs.DTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostResponseDTO implements DTO {
    private int id;
    private String description;
    private String title;
    private float price;
    private boolean active;
    private Instant createdAt;
    private Instant expiresAt;
    private int viewsNo;
    private Set<ImageDTO> images = new HashSet<>();
    private Set<CommentDTO> comments = new HashSet<>();
    private UserMinifiedDTO user;
    private PostCategoryDTO category;
    private Long timeLeftDays;
    private Long timeLeftHours;
    private Long timeLeftMinutes;
    private Boolean liked;
}
