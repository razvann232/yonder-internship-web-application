package com.example.yoleicsbebogdan.DTOs.Responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostMediumWithLikedDTO extends PostMediumDTO{
    private Boolean liked;
}
