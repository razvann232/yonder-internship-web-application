package com.example.yoleicsbebogdan.DTOs.Requests;

import com.example.yoleicsbebogdan.DTOs.DTO;
import com.example.yoleicsbebogdan.DTOs.IUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRegisterDTO implements DTO, IUser {

    private String name;
    private String email;
    private String phoneNumber;
    private String password;

}
