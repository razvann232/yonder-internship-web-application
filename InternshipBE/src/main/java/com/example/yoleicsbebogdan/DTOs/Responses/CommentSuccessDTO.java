package com.example.yoleicsbebogdan.DTOs.Responses;

import com.example.yoleicsbebogdan.DTOs.DTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentSuccessDTO implements DTO {
    private Integer id;
    private String content;
    private Instant date;
    private UserMinifiedDTO user;
    private PostSmallDTO post;
}