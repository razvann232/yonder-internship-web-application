package com.example.yoleicsbebogdan.Utils.Validators;

import com.example.yoleicsbebogdan.DTOs.IUser;
import com.example.yoleicsbebogdan.Entities.User;
import com.example.yoleicsbebogdan.Exceptions.UserAuthException;
import com.example.yoleicsbebogdan.Exceptions.UserValidationException;
import com.example.yoleicsbebogdan.Repositories.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;
import java.util.regex.Pattern;

@Component
@AllArgsConstructor
public class UserValidator {

    private final UserRepository userRepository;

    public User authUser(Integer id) throws UserAuthException {
        Optional<User> authorOptional = userRepository.findById(id);

        if (authorOptional.isEmpty()) {
            String message = "Session expired. You need to login or register if you want to access this content!";
            throw new UserAuthException(message);
        }
        return authorOptional.get();
    }

    public void validateUser(IUser user) throws UserValidationException {
        validateUser(user, false);
    }

    public void validateUser (IUser user, Boolean ignorePassword) throws UserValidationException {
        try{
            Class<? extends IUser> userClass = user.getClass();
            for (Field f : userClass.getDeclaredFields()) {
                if("name".equals(f.getName())){
                    String name = (String) getValueOfField(f.getName(),user);
                    validateName(name);
                }
                else if("email".equals(f.getName())){
                    String email = (String) getValueOfField(f.getName(),user);
                    validateEmail(email);
                }
                else if("phoneNumber".equals(f.getName())){
                    String phoneNumber = (String) getValueOfField(f.getName(),user);
                    validatePhoneNumber(phoneNumber);
                }
                else if(!ignorePassword && "password".equals(f.getName())){
                    String password = (String) getValueOfField(f.getName(),user);
                    validatePassword(password);
                }
            }
        }catch(IntrospectionException | InvocationTargetException | IllegalAccessException e){
            e.printStackTrace();
            throw new UserValidationException("Could not do validation! :(");
        }
    }
    public void validateName(String name) throws UserValidationException {
        if(!Pattern.matches("([\\w ]+)",name)){
            throw new UserValidationException("Invalid user name!");
        }
    }
    public void validateEmail(String email) throws UserValidationException {
        if(!Pattern.matches("\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})",email)){
            throw new UserValidationException("Invalid email!");
        }
    }
    public void validatePhoneNumber(String phoneNum) throws UserValidationException {
        if(!Pattern.matches("0\\d{9}",phoneNum)){
            throw new UserValidationException("Invalid phone number!");
        }
    }
    public void validatePassword(String password) throws UserValidationException {
        if(!Pattern.matches("(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$",password)){
            throw new UserValidationException("Password must contain an uppercase letter, lowercase letter, a digit and a special character!");
        }
    }

    private Object getValueOfField(String nameOfField, Object obj) throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        PropertyDescriptor propertyDescriptor = new PropertyDescriptor(nameOfField,obj.getClass());
        Method getter = propertyDescriptor.getReadMethod();
        return getter.invoke(obj);
    }
}
