package com.example.yoleicsbebogdan.Utils;

import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;

@Component
public class TimeBetweenHelper {
    public Long[] getDaysHoursMinutesBetween(Instant start, Instant end){
        Duration duration = Duration.between(start,end);
        long totalSeconds = duration.getSeconds();

        long days = totalSeconds / (3600 * 24);
        totalSeconds = totalSeconds % (3600 * 24);

        long hours = totalSeconds / 3600;
        totalSeconds = totalSeconds % 3600;

        long minutes = totalSeconds / 60;
        return new Long[] {days,hours,minutes};
    }
}
