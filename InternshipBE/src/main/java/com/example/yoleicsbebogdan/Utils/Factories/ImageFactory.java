package com.example.yoleicsbebogdan.Utils.Factories;

import com.example.yoleicsbebogdan.Entities.Image;
import com.example.yoleicsbebogdan.Entities.ProfilePicture;
import com.example.yoleicsbebogdan.Entities.Thumbnail;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@Component
@AllArgsConstructor
public class ImageFactory {
    private final ModelMapper modelMapper;

    public Set<Image> mapMultipartFileArrayToImageSet(MultipartFile[] files){
        Set<Image> images = new HashSet<>();
        for (MultipartFile file : files) {
            try {
                Image newImage = createNewImage(file);
                images.add(newImage);
            } catch (IOException ignored) {
            }
        }
        return images;
    }

    public Image createNewImage(MultipartFile file) throws IOException {
        byte[] content = file.getBytes();
        String typeName = file.getContentType();
        Image image = new Image();
        image.setContent(content);
        image.setType(typeName);
        return image;
    }

    public ProfilePicture createNewProfilePicture(MultipartFile file) throws IOException {
        return modelMapper.map(createNewImage(file),ProfilePicture.class);
    }


    public Thumbnail generateThumbnail(Image image){
        Thumbnail thumbnail = new Thumbnail();
        byte[] content = scale(image.getContent(),260,200);
        thumbnail.setContent(content);
        thumbnail.setType("image/jpg");
        thumbnail.setId(image.getId());
        return thumbnail;
    }

    private byte[] scale(byte[] fileData, int width, int height) {
        ByteArrayInputStream in = new ByteArrayInputStream(fileData);
        try {
            BufferedImage img = ImageIO.read(in);

            java.awt.Image scaledImage = img.getScaledInstance(width, height,java.awt.Image.SCALE_SMOOTH);
            BufferedImage imageBuff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            imageBuff.getGraphics().drawImage(scaledImage, 0, 0, new Color(255, 255, 255), null);

            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            ImageIO.write(imageBuff, "jpg", buffer);
            return buffer.toByteArray();
        } catch (IOException e){
            System.out.println("ERROR in the Image scaling method");
        }
        return null;
    }
}
