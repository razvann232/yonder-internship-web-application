package com.example.yoleicsbebogdan.Utils.Factories;

import com.example.yoleicsbebogdan.DTOs.Requests.UserRegisterDTO;
import com.example.yoleicsbebogdan.DTOs.Requests.UserUpdateDTO;
import com.example.yoleicsbebogdan.Entities.Permissions;
import com.example.yoleicsbebogdan.Entities.Role;
import com.example.yoleicsbebogdan.Entities.RoleEnum;
import com.example.yoleicsbebogdan.Entities.User;
import com.example.yoleicsbebogdan.Exceptions.UserValidationException;
import com.example.yoleicsbebogdan.Utils.Validators.UserValidator;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
@AllArgsConstructor
public class UserFactory {
    private final PasswordEncoder bcrypt;
    private final ModelMapper mapper;
    private final UserValidator userValidator;

    public User createNewUser(UserRegisterDTO user) {
        User newUser = mapper.map(user, User.class);
        String hashedPassword = bcrypt.encode(user.getPassword());
        newUser.setPassword(hashedPassword);

        Role role = new Role();
        Permissions permission = new Permissions();
        permission.setPermission("User operations");
        permission.setDescription("Can do self CRUD operations on his account and his items");
        permission.setRole(role);
        Set<Permissions> permissionsSet = new HashSet<>();
        permissionsSet.add(permission);
        role.setPermissions(permissionsSet);
        role.setRoleName(RoleEnum.USER);
        role.setDesc("The user is the app's client");
        newUser.setRole(role);
        return newUser;
    }

    public User updateExistingUser(User user, UserUpdateDTO newData) throws UserValidationException {
        if (newData.getEmail() != null) {
            user.setEmail(newData.getEmail());
        }
        if (newData.getName() != null) {
            user.setName(newData.getName());
        }
        if (newData.getPhoneNumber() != null) {
            user.setPhoneNumber(newData.getPhoneNumber());
        }
        if (newData.getPassword() != null) {
            userValidator.validatePassword(newData.getPassword());
            String hashedPassword = bcrypt.encode(newData.getPassword());
            user.setPassword(hashedPassword);
        }
        if (newData.getGender() != null) {
            user.setGender(newData.getGender());
        }
        return user;
    }
}
