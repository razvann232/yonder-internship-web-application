package com.example.yoleicsbebogdan.Utils.Validators;

import com.example.yoleicsbebogdan.Configs.CommentValidationConstants;
import com.example.yoleicsbebogdan.DTOs.Requests.NewCommentDTO;
import com.example.yoleicsbebogdan.Exceptions.CommentValidationException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class CommentValidator {
    public void validateComment(NewCommentDTO comment) throws CommentValidationException {
        if(comment == null || comment.getContent().length() > CommentValidationConstants.COMMENT_MAX_LENGTH){
            throw new CommentValidationException("Comment length cannot be longer than "+CommentValidationConstants.COMMENT_MAX_LENGTH+" characters!");
        }
    }
}
