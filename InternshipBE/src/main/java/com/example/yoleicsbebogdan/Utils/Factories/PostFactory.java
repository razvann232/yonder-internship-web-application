package com.example.yoleicsbebogdan.Utils.Factories;

import com.example.yoleicsbebogdan.DTOs.DTO;
import com.example.yoleicsbebogdan.DTOs.Requests.NewPostDTO;
import com.example.yoleicsbebogdan.DTOs.Responses.PostMediumDTO;
import com.example.yoleicsbebogdan.DTOs.Responses.PostMediumWithActiveDTO;
import com.example.yoleicsbebogdan.Entities.*;
import com.example.yoleicsbebogdan.Utils.TimeBetweenHelper;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class PostFactory {
    private final ModelMapper mapper;
    private final ImageFactory imageFactory;
    private final TimeBetweenHelper timeBetweenHelper;

    public PostFactory(ImageFactory imageFactory, TimeBetweenHelper timeBetweenHelper) {
        this.imageFactory = imageFactory;
        this.timeBetweenHelper = timeBetweenHelper;
        this.mapper = new ModelMapper();
        mapper.addMappings(new PropertyMap<Post, NewPostDTO>() {
            @Override
            protected void configure() {
                skip(destination.getImages());
            }
        });
    }

    public Post createNewPost(NewPostDTO newPostDTO, User user) {
        Post newPost = mapper.map(newPostDTO, Post.class);
        newPost.setCreatedAt(Instant.now());
        newPost.setActive(true);
        PostCategory type = new PostCategory();
        type.setDescription("Postul este de tipul: " + newPostDTO.getCategory().toString());
        type.setType(newPostDTO.getCategory());
        type.setPost(newPost);
        newPost.setCategory(type);
        newPost.setViewsNo(0);
        newPost.setUser(user);

        Set<Image> images = imageFactory.mapMultipartFileArrayToImageSet(newPostDTO.getImages());
        images.forEach(image -> image.setPost(newPost));

        Thumbnail thumbnail = imageFactory.generateThumbnail(images.iterator().next());
        thumbnail.setPost(newPost);
        newPost.setThumbnail(thumbnail);
        newPost.setImages(images);

        return newPost;
    }

    public List<DTO> mapPostsToMediumDTO(Set<Post> posts) {
        List<DTO> postsDTO = new ArrayList<>();

        posts.forEach(post -> {
            PostMediumDTO postDTO = mapPostToMediumDTO(post);
            postsDTO.add(postDTO);
        });

        return postsDTO;
    }

    public List<DTO> mapPostsToMediumWithActiveDTO(Set<Post> posts) {
        List<DTO> postsDTO = new ArrayList<>();

        posts.forEach(post -> {
            PostMediumWithActiveDTO postDTO = mapper.map(mapPostToMediumDTO(post), PostMediumWithActiveDTO.class);
            postDTO.setActive(post.isActive());
            postsDTO.add(postDTO);
        });

        return postsDTO;
    }

    public PostMediumDTO mapPostToMediumDTO(Post post) {
        PostMediumDTO postDTO = mapper.map(post, PostMediumDTO.class);
        Long[] daysHoursMinutes = timeBetweenHelper.getDaysHoursMinutesBetween(post.getCreatedAt(), post.getExpiresAt());
        postDTO.setTimeLeftDays(daysHoursMinutes[0]);
        postDTO.setTimeLeftHours(daysHoursMinutes[1]);
        postDTO.setTimeLeftMinutes(daysHoursMinutes[2]);
        return postDTO;
    }

    public Post updateOldPost(Post post, NewPostDTO formData) {
        mapper.map(formData, post);
        Set<Image> images = imageFactory.mapMultipartFileArrayToImageSet(formData.getImages());
        images.forEach(image->image.setPost(post));
        post.setImages(images);

        PostCategory type = new PostCategory();
        type.setDescription("Postul este de tipul: " + formData.getCategory().toString());
        type.setType(formData.getCategory());
        type.setPost(post);
        post.setCategory(type);

        Thumbnail thumbnail = imageFactory.generateThumbnail(images.iterator().next());
        thumbnail.setPost(post);
        post.setThumbnail(thumbnail);

        return post;
    }
}
