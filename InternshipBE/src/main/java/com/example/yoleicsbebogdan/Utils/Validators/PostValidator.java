package com.example.yoleicsbebogdan.Utils.Validators;

import com.example.yoleicsbebogdan.Configs.PostValidationConstants;
import com.example.yoleicsbebogdan.DTOs.Requests.NewPostDTO;
import com.example.yoleicsbebogdan.Entities.Post;
import com.example.yoleicsbebogdan.Entities.PostCategoryEnum;
import com.example.yoleicsbebogdan.Exceptions.PostValidationException;
import com.example.yoleicsbebogdan.Repositories.PostRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.time.Instant;
import java.util.Optional;

@Component
@AllArgsConstructor
public class PostValidator {
    private final PostRepository postRepository;

    public Post checkIfPostExists(Integer postId) throws PostValidationException{
        Optional<Post> response = postRepository.findById(postId);
        if (response.isEmpty()) {
            throw new PostValidationException("The post with the given id does not exist!");
        }
        return response.get();
    }

    public void validatePost(NewPostDTO post) throws PostValidationException {
        validateTitle(post.getTitle());
        validateDescription(post.getDescription());
        validateType(post.getCategory());
        validatePrice(post.getPrice(),post.getCategory());
        validateDate(post.getExpiresAt());
        validateImages(post.getImages());
    }

    public void validateImages(MultipartFile[] images) throws PostValidationException{
        for(MultipartFile file : images){
            if(file.getContentType() == null || !file.getContentType().contains("image/")){
                throw new PostValidationException("File \""+file.getOriginalFilename()+"\" is not an image!");
            }
        }
    }

    public void validateTitle(String title) throws PostValidationException {
        if (title == null || title.length() < PostValidationConstants.TITLE_MIN_LEN) {
            throw new PostValidationException("Title too short. Name must have at least " + PostValidationConstants.TITLE_MIN_LEN + " characters");
        }
        if (title.length() > PostValidationConstants.TITLE_MAX_LEN) {
            throw new PostValidationException("Title too long. Name must have a maximum of " + PostValidationConstants.TITLE_MAX_LEN + " characters");
        }
    }

    public void validateDescription(String description) throws PostValidationException {
        if (description == null || description.length() < PostValidationConstants.DESCRIPTION_MIN_LEN) {
            throw new PostValidationException("Description too short. Description must have at least " + PostValidationConstants.DESCRIPTION_MIN_LEN + " characters");
        }
        if (description.length() > PostValidationConstants.DESCRIPTION_MAX_LEN) {
            throw new PostValidationException("Description too long. Description must have a maximum of " + PostValidationConstants.DESCRIPTION_MIN_LEN + " characters");
        }
    }

    public void validateDate(Instant date) throws PostValidationException {
        if (date == null) {
            throw new PostValidationException("Date cannot be null!");
        }
        if (date.isBefore(Instant.now())) {
            throw new PostValidationException("Date must be in the future!");
        }
    }

    public void validateType(PostCategoryEnum type) throws PostValidationException {
        if (type == null) {
            throw new PostValidationException("Type cannot be null!");
        }
    }

    public void validatePrice(Float price, PostCategoryEnum type) throws PostValidationException {
        if (!type.equals(PostCategoryEnum.EXCHANGE)) {
            if (price <= 0) {
                throw new PostValidationException("Price must be greater than 0!");
            }
        }
        else{
            if (price != 0) {
                throw new PostValidationException("Price must be 0 for EXCHANGE posts!");
            }
        }
    }

}
