package com.example.yoleicsbebogdan.Controllers;

import com.example.yoleicsbebogdan.DTOs.DTO;
import com.example.yoleicsbebogdan.DTOs.Requests.UserLoginDTO;
import com.example.yoleicsbebogdan.DTOs.Requests.UserRegisterDTO;
import com.example.yoleicsbebogdan.DTOs.Responses.UserDTO;
import com.example.yoleicsbebogdan.Entities.User;
import com.example.yoleicsbebogdan.Exceptions.UserNotFoundException;
import com.example.yoleicsbebogdan.Exceptions.UserValidationException;
import com.example.yoleicsbebogdan.Repositories.UserRepository;
import com.example.yoleicsbebogdan.Utils.Factories.UserFactory;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
class UserControllerTest {

    @MockBean
    private UserRepository mockedUserRepository;
    @Autowired
    private UserController controller;
    @Autowired
    private UserFactory userFactory;
    @Autowired
    private ModelMapper mapper;

    private final UserRegisterDTO validUserInput = new UserRegisterDTO(
            "Barbu",
            "foobar@foobar.com",
            "0123456783",
            "FooBar2022!");

    @Test
    void registerInvalidEmailTest() {
        UserRegisterDTO invalidEmailUserInput = new UserRegisterDTO("Mihai", "pineappleyahoo.com", "0123456789", "StrongPass00!");

        assertThrows(UserValidationException.class, () -> {
            controller.postUser(invalidEmailUserInput);
        });
    }

    @Test
    void registerInvalidPhoneTest() {
        UserRegisterDTO invalidPhoneNumberInput = new UserRegisterDTO("Alina", "pearjuice@app.com", "0341dg4", "StrongPass00!");

        assertThrows(UserValidationException.class, () -> {
            controller.postUser(invalidPhoneNumberInput);
        });
    }

    @Test
    void registerInvalidPasswordTest() {
        UserRegisterDTO invalidPasswordNumber = new UserRegisterDTO("George", "battlewarrior@game.com", "0720498756", "lowpassword:(");

        assertThrows(UserValidationException.class, () -> {
            controller.postUser(invalidPasswordNumber);
        });
    }


    @Test
    void registerValidUserTest() throws UserValidationException {
        User validUserInsertedAndRetrieved = userFactory.createNewUser(validUserInput);
        when(mockedUserRepository.save(any())).thenReturn(validUserInsertedAndRetrieved);
        ResponseEntity<DTO> response = controller.postUser(validUserInput);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody()).isEqualTo(mapper.map(validUserInsertedAndRetrieved, UserDTO.class));
    }

    @Test
    void registerAlreadyExistingUser() {
        UserRegisterDTO alreadyExistingUser = new UserRegisterDTO(
                "Bogdan",
                "foo.bar@foobar.com",
                "0123456789",
                "FooBar@2022");

        when(mockedUserRepository.save(any())).thenThrow(new DataIntegrityViolationException("Message"));

        assertThrows(DataIntegrityViolationException.class, () -> {
            controller.postUser(alreadyExistingUser);
        });
    }

    @Test
    void loginValidUser() throws UserNotFoundException, UserValidationException {
        UserLoginDTO alreadyExistingUserCredentials = new UserLoginDTO("foobar@foobar.com", "FooBar2022!");

        User validUser = userFactory.createNewUser(validUserInput);
        when(mockedUserRepository.findByEmail(any())).thenReturn(Optional.of(validUser));

        ResponseEntity<DTO> response = controller.loginUser(alreadyExistingUserCredentials);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.ACCEPTED);
        assertThat(response.getBody()).isEqualTo(mapper.map(validUser, UserDTO.class));
    }

    @Test
    void loginUnregisteredUser() {
        UserLoginDTO randomUserCredentials = new UserLoginDTO("unexistinguser@foobar.com", "FooBar2022!");

        when(mockedUserRepository.findByEmail(any())).thenReturn(Optional.empty());

        assertThrows(UserNotFoundException.class, () -> {
            controller.loginUser(randomUserCredentials);
        });
    }

    @Test
    void loginInvalidEmail() {
        UserLoginDTO invalidEmailUserCredentials = new UserLoginDTO("email.com", "FooBar2022!");

        assertThrows(UserValidationException.class, () -> {
            controller.loginUser(invalidEmailUserCredentials);
        });
    }
}