## An e-commerce web application
It was done throughout the internship at Yonder in a team of five. Below are some screenshots with explainations on top and whether or not they were implemented by me:


The login page:
![Login page](https://gitlab.com/razvann232/images/-/raw/main/login.png)

The menu (done by me):
![Menu](https://gitlab.com/razvann232/images/-/raw/main/menu.png)

The sell and buy page (done by me):
![Sell and buy](https://gitlab.com/razvann232/images/-/raw/main/Sell_buy.png)

The profile page:
![Profile](https://gitlab.com/razvann232/images/-/raw/main/profile_pager.png)

The product page(done by me):
![Product page](https://gitlab.com/razvann232/images/-/raw/main/product_page.png)

The add product page:
![Add Product](https://gitlab.com/razvann232/images/-/raw/main/addprod.png)

The "My posts" page:
![My posts](https://gitlab.com/razvann232/images/-/raw/main/products_page.png)